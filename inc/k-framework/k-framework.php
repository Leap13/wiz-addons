<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access directly.

/**
 *
 * @package   Wiz Framework - WordPress Options Framework
 * @author    Leap13
 * @link      https://wiz.io
 * @copyright 2019 Leap13
 *
 *
 * Plugin Name: Wiz Framework
 * Plugin URI: https://wiz.io
 * Author: Leap13
 * Author URI: https://leap13.com
 * Version: 0.1.0
 * Description: K Framework is a lightweight WordPress options framework developed to be used for Wiz WordPress Theme and addon plugins.
 * Text Domain: kfw
 * Domain Path: /languages
 *
 */

require_once plugin_dir_path( __FILE__ ) .'classes/setup.class.php';
