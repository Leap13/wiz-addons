<?php
/**
 * Wiz Addons Class.
 */

if ( ! class_exists('Wiz_Addons' ) ) {
    
    /**
	 * Wiz Theme Addons initial setup
     * 
     */
    class Wiz_Addons {
        /*
         * @var instance
        */

        private static $instance;

        /**
         *  Initiator.
         */
        public static function get_instance()
        {
            if (!isset(self::$instance)) {
                self::$instance = new self();
            }
        }

        /**
         * Constructor.
         */
        public $plugin;

        public function __construct() {

            // Included Files
            $this->includes();
            
            // Activation hook.
			register_activation_hook( WIZ_ADDONS_FILE, array( $this, 'activation' ) );

			// deActivation hook.
			register_deactivation_hook( WIZ_ADDONS_FILE, array( $this, 'deactivation' ) );

            add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ) );
            
            require_once WIZ_ADDONS_DIR.'inc/wiz-addons-settings.php';

            add_action('after_setup_theme', array($this, 'setup'));
            add_action( 'admin_enqueue_scripts', array($this, 'wiz_admin_styles'));
        }

        public function activation() {
            //registered KA
            //Flush rewrite rules
            flush_rewrite_rules();
        }

        /**
         * After Setup Theme.
         */
        public function setup() {
            if (!defined('WIZ_THEME_VERSION')) {
                return;
            }

            require_once WIZ_ADDONS_DIR.'classes/class-wiz-addons-activate.php';
        }

        public function deactivation() {
            //Flush rewrite rules
            flush_rewrite_rules();
        }
        
        /**
		 * Includes
		 */
		function includes() {
            require_once WIZ_ADDONS_DIR.'classes/class-wiz-style-generator.php';
            require_once WIZ_ADDONS_DIR.'inc/k-framework/k-framework.php';
            require_once WIZ_ADDONS_DIR.'inc/functions.php';
        }
        
        public function load_plugin_textdomain() {
            load_plugin_textdomain('wiz-addons', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/');
        }
        
        public function wiz_admin_styles() {
            wp_enqueue_style( 'wiz_admin_css', WIZ_ADDONS_URL .'assets/admin/css/style.css' );
        }  

    }
}

Wiz_Addons::get_instance();

