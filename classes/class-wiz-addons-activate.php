<?php 

/**
 * Enable Addons
 *
 * @since 1.0
 */
final class Wiz_Addon_Activate {

	/**
	 * Construct
	 */
	function __construct() {

		if ('wiz' == get_template()){
			$this->load_wiz_addons();
		}
    }
	

	/**
	 * Load Wiz Addons
	 *
	 * @return void
	 */
	function load_wiz_addons() {
		
        $enabled_addon = get_option( 'wiz_framework' );

		if ( 0 < count( $enabled_addon ) ) {

			foreach ( $enabled_addon as $slug => $value ) {

				if ( false == $value ) {
					continue;
				}
                
                $addon_path = WIZ_ADDONS_DIR . 'addons/' . esc_attr( $slug ) . '/class-wiz-addon-' . esc_attr( $slug ) . '.php';

				if ( file_exists( $addon_path ) ) {
					require_once $addon_path;
				}
			}
		}
	}
}

new Wiz_Addon_Activate();