<?php
/**
 * Plugin Name: Wiz Addons
 * Plugin URI: https://wiz.io
 * Description: This Plugin for Wiz Theme
 * Version: 0.9.0
 * Author: Leap13
 * Author URI: https://leap13.com
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: wiz-addons
 * Domain Path: /languages
 * License: GNU General Public License v3.0.
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if ('wiz' !== get_template()) {
    return;
}

/*
 * Set constants.
 */

define( 'WIZ_ADDONS_FILE', __FILE__ );
define( 'WIZ_ADDONS_BASE', plugin_basename( WIZ_ADDONS_FILE));
define( 'WIZ_ADDONS_VERSION', '0.9.0' );
define( 'WIZ_ADDONS_URL', plugins_url('/', WIZ_ADDONS_FILE));
define( 'WIZ_ADDONS_DIR', plugin_dir_path(WIZ_ADDONS_FILE));

/*
 * Main Wiz Addons
 */

require_once WIZ_ADDONS_DIR.'classes/class-wiz-addons.php';
