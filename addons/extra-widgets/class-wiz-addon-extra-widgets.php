<?php
/**
 * Wiz Extra Widgets
 *
 * @package Wiz Addons
 */

define( 'WIZ_WIDGETS_DIR', WIZ_ADDONS_DIR . 'addons/extra-widgets/' );
define( 'WIZ_WIDGETS_URL', WIZ_ADDONS_URL . 'addons/extra-widgets/' );

if ( ! class_exists( 'Wiz_Extra_Widgets' ) ) {

	/**
	 * Widgets Setup
	 *
	 * @since 1.0.0
	 */
	class Wiz_Extra_Widgets {

		/**
		 * Member Variable
		 *
		 * @var object instance
		 */
		private static $instance;

		/**
		 *  Initiator
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 *  Constructor
		 */
		
		public function __construct() {

			require_once WIZ_WIDGETS_DIR . 'classes/class-create-widgets.php';
			require_once WIZ_WIDGETS_DIR . 'classes/class-widgets-partials.php';

		}

	}

    Wiz_Extra_Widgets::get_instance();
}