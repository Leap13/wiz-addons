<?php
/**
 * Widgets Partials
 * 
 * @package Wiz Addons
 */
if (! class_exists('Wiz_Extra_Widgets_Partials')) {

    class Wiz_Extra_Widgets_Partials {

        private static $instance;

        /**
         * Initiator
         */
        
        public static function get_instance()
        {
            if (! isset(self::$instance)) {
                self::$instance = new self();
            }
            return self::$instance;
        }
        /**
		 *  Constructor
		 */
		public function __construct() {
            require_once WIZ_WIDGETS_DIR . 'classes/helper.php'; 
            add_action( 'wiz_get_css_files', array( $this, 'add_styles' ) );
            add_action( 'wiz_get_js_files', array( $this, 'add_scripts' ) );
            add_action( 'widgets_init', array( $this, 'wiz_extra_widgets_markup'), 10 );
        }
        
        public static function wiz_extra_widgets_markup() {

            // Define array of custom widgets for the theme
            $widgets = apply_filters( 'wiz_custom_widgets', array(
                'mailchimp',
                'social-icons',
            ) );

            // Loop through widgets and load their files
            if ( $widgets && is_array( $widgets ) ) {
                foreach ( $widgets as $widget ) {
                    $file = WIZ_WIDGETS_DIR . 'widgets/' . $widget .'.php';
                    if ( file_exists ( $file ) ) {
                        require_once( $file );
                    }
                }
            }
        }
        
        public function add_styles() {
            Wiz_Style_Generator::wiz_add_css(WIZ_WIDGETS_DIR.'assets/css/minified/style.min.css');
        }
        public function add_scripts() {
             Wiz_Style_Generator::wiz_add_js(WIZ_WIDGETS_DIR.'assets/js/minified/mailchimp.min.js');
		}
        
    }
}
Wiz_Extra_Widgets_Partials::get_instance();
