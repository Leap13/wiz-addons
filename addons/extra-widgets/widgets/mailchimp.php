<?php
/**
 * Mailchimp Widget.
 *
 * @package Wiz Addons
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Start class
if ( ! class_exists( 'Wiz_MailChimp_Widget' ) ) {
    class Wiz_MailChimp_Widget extends WP_Widget {
 
    public function __construct() {
        parent::__construct(
            'wiz-widget-mailchimp',
            esc_html__('Wiz Mailchimp', 'wiz-addons'),
            array( 'description' => esc_html__('Mailchimp subscribe widget', 'wiz-addons'))
        );
    }
 
    public function widget( $args, $instance ) {
 
        extract($args);
 
        $title  = apply_filters( 'widget_title', $instance['title'] );
        $list   = wiz_get_panel_option('wiz-mailchimp-list-id');

        $output = "";
        $output .= $before_widget;
        $output .='<div class="mailchimp-form">';
            if ( ! empty( $title ) ){$output .= $before_title . $title . $after_title;}
             
            $output .='<form class="wiz-mailchimp-form" name="wiz-mailchimp-form" action="'.esc_url( admin_url('admin-post.php') ).'" method="POST">';
                $output .='<div>';
                    $output .='<input type="text" value="" name="email" placeholder="'.esc_html__("Email", 'wiz-addons').'">';
                    $output .='<span class="alert warning">'.esc_html__('Invalid or empty email', 'wiz-addons').'</span>';
                $output .= '</div>';
                 
                $output .='<div class="send-div">';
                    $output .='<input type="submit" class="button" value="'.esc_html__('Subscribe', 'wiz-addons').'" name="subscribe">';
                    $output .='<div class="sending"></div>';
                $output .='</div>';
 
                $output .='<div class="wiz-mailchimp-success alert final success">'.esc_html__('You have successfully subscribed to the newsletter.', 'wiz-addons').'</div>';
                $output .='<div class="wiz-mailchimp-error alert final error">'.esc_html__('Something went wrong. Your subscription failed.', 'wiz-addons').'</div>';
                 
                $output .='<input type="hidden" value="'.$list.'" name="list">';
                $output .='<input type="hidden" name="action" value="wiz_mailchimp" />';
                $output .= wp_nonce_field( "wiz_mailchimp_action", "wiz_mailchimp_nonce", false, false );
 
            $output .='</form>';
        $output .='</div>';
        $output .= $after_widget;
        echo $output;

    }
 
    public function form( $instance ) {
        $defaults = array(
            'title' => esc_html__('Subscribe', 'wiz-addons'),
        );
 
        $instance = wp_parse_args((array) $instance, $defaults);
        $title	  = isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : '';
        ?>
            <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo esc_html__( 'Title:', 'wiz-addons' ); ?></label> 
                <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" />
            </p>
    <?php }
    }
 
}
register_widget( 'Wiz_MailChimp_Widget' );