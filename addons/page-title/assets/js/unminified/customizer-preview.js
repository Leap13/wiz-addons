/**
	 * Page Title customizer preview
	 */
(function ($) {

    /**
	 * Page Title background
	 */
    wp.customize('wiz-settings[page-title-bg-obj]', function (value) {
        value.bind(function (bg_obj) {
            var dynamicStyle = ' .wiz-page-title-addon-content, .wiz-merged-header-title { {{css}} }';
            wiz_background_obj_css(wp.customize, bg_obj, 'header-bg-obj', dynamicStyle);
        });
    });
    wp.customize('wiz_breadcrumb_separator', function (value) {
        value.bind(function (newval) {
            $('.wiz-breadcrumbs-trail ul li .breadcrumb-sep').text(newval);
        });
    });
    wiz_responsive_spacing('wiz-settings[page-title-space]', '.wiz-page-title-addon-content', 'padding', ['top', 'right', 'bottom', 'left']);
    wiz_css('wiz-settings[page-title-color]', 'color', '.wiz-page-title');
    wiz_responsive_slider('wiz-settings[page-title-font-size]', '.wiz-page-title' , 'font-size');
    wiz_css('wiz-settings[page-title-border-right-color]', 'border-right-color', '.page-title-layout-3 .wiz-page-title-wrap');
    wiz_css('wiz-settings[pagetitle-text-transform]', 'text-transform', '.wiz-page-title');
    wiz_css('wiz-settings[pagetitle-line-height]', 'line-height', '.wiz-page-title');
    //Page Title Bottom Title
    wiz_css('wiz-settings[pagetitle-bottomline-height]', 'height', '.wiz-page-title::after', 'px');
    wiz_css('wiz-settings[pagetitle-bottomline-width]', 'width', '.wiz-page-title::after', 'px');
    wiz_css('wiz-settings[pagetitle-bottomline-color]', 'background-color', '.wiz-page-title::after');
    // Breadcrumbs 
    wiz_responsive_spacing('wiz-settings[breadcrumbs-space]', '.wiz-breadcrumb-trail', 'padding', ['top', 'right', 'bottom', 'left']);
    wiz_css('wiz-settings[breadcrumbs-color]', 'color', '.wiz-breadcrumb-trail span');
    wiz_css('wiz-settings[breadcrumbs-link-color]', 'color', '.wiz-breadcrumb-trail a span');
    wiz_css('wiz-settings[breadcrumbs-link-h-color]', 'color', '.wiz-breadcrumb-trail a:hover span');


})(jQuery);