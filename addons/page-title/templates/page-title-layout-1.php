<?php

/**
 * Page Title
 *
 * @package Wiz Addon
 */

$title                 = wiz_get_the_title();
$description           = get_the_archive_description();
$classes []= wiz_get_option( 'page-title-layouts' );
$classes_responsive = wiz_get_option( 'page-title-responsive' );
if ( "page-title-layout-1" === wiz_get_option( 'page-title-layouts' )) {
	$classes []= wiz_get_option( 'page_title_alignment' );
}

$classes   = implode( ' ', $classes );

?>
<div class="wiz-page-title-addon-content <?php echo esc_attr( $classes_responsive); ?>">
	<div class="wiz-page-title <?php echo esc_attr( $classes); ?>" >
		<div class="wiz-container">
			<div class="wiz-page-title-wrap">
				<?php if ( $title ) { ?>
				<h1 class="wiz-page-title">
					<?php echo apply_filters( 'wiz_page_title_addon_title', wp_kses_post( $title ) ); ?>
				</h1>
				<?php } ?>
				<?php if ( $description ) { ?>
				<div class="taxonomy-description">
					<?php echo apply_filters( 'wiz_page_title_addon_description', wp_kses_post( $description ) ); ?>
				</div>
				<?php } ?>
			</div>
	<?php if ( apply_filters( 'wiz_the_page_title_enabled', true ) ) { ?>
			<?php wiz_breadcrumb_trail() ?>
	<?php } ?>
		</div>
	</div>
</div>
