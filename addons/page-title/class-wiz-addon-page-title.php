<?php
/**
 * Page Title Section
 * 
 * @package Wiz Addons
 */

define( 'WIZ_PAGE_TITLE_DIR', WIZ_ADDONS_DIR . 'addons/page-title/' );
define( 'WIZ_PAGE_TITLE_URL', WIZ_ADDONS_URL . 'addons/page-title/' );

if ( ! class_exists( 'Wiz_Page_Title' ) ) {

	class Wiz_Page_Title {

		/**
		 * Member Variable
		 *
		 * @var object instance
		 */
		private static $instance;

		/**
		 *  Initiator
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 *  Constructor
		 */
		
		public function __construct() {

            require_once WIZ_PAGE_TITLE_DIR . 'classes/class-page-title-settings.php';
			require_once WIZ_PAGE_TITLE_DIR . 'classes/class-page-title-partials.php';
			require_once WIZ_PAGE_TITLE_DIR . 'classes/class-wiz-breadcrumbs.php';
            
            if ( ! is_admin() ) {
				require_once WIZ_PAGE_TITLE_DIR . 'classes/dynamic.css.php';
			}
		}
       
		

	}
    Wiz_Page_Title::get_instance();
}
