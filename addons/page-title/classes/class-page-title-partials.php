<?php
/**
 * Page Title Section
 * 
 * @package Wiz Addons
 */
if (! class_exists('Wiz_Page_Title_Partials')) {

    /**
     * Page Title Section
     *
     * @since 1.0.0
     */
    class Wiz_Page_Title_Partials {

        private static $instance;
        
        public static function get_instance()
        {
            if (! isset(self::$instance)) {
                self::$instance = new self();
            }
            return self::$instance;
        }
        /**
		 *  Constructor
		 */
		public function __construct() {
            add_filter( 'wiz_title_bar_disable', '__return_false' );
            add_action( 'wiz_after_header_block' , array( $this, 'wiz_page_title_markup' ), 9 );
            add_action( 'wiz_get_css_files', array( $this, 'add_styles' ) );
            add_action( 'wiz_before_header_block', array( $this, 'header_merged_with_title' ) );
        }

        public function wiz_page_title_markup() {
            $page_title_layout = wiz_get_option( 'page-title-layouts' );
            if ( apply_filters( 'wiz_the_page_title_enabled', true ) ) {
                if($page_title_layout !== 'page-title-layout-2'){
                    wizaddons_get_template( 'page-title/templates/'. esc_attr( $page_title_layout ) . '.php' );
                }else{
                    wizaddons_get_template( 'page-title/templates/page-title-layout-1.php' );
                }
            }
            $header_merged_title = wiz_get_option('merge-with-header');
            if( $header_merged_title == '1') {
                echo '</div>';
            }
         }

        public function header_merged_with_title() {
            $header_merged_title = wiz_get_option("merge-with-header");
            if( $header_merged_title == '1') {
                $combined = 'wiz-merged-header-title';
            printf(
				'<div class="%1$s">',
				$combined
            );
            }
            
        }

        function add_styles() {
            Wiz_Style_Generator::wiz_add_css( WIZ_PAGE_TITLE_DIR.'assets/css/minified/style.min.css');

	    }

    }
}
Wiz_Page_Title_Partials::get_instance();
