<?php
/**
 * Page Title addon - Dynamic CSS
 * 
 * @package Wiz Addons
 */

add_filter( 'wiz_dynamic_css', 'wiz_ext_page_title_dynamic_css');

function wiz_ext_page_title_dynamic_css( $dynamic_css ) {
            $page_title_bg        = wiz_get_option( 'page-title-bg-obj' );
            $page_title_space        = wiz_get_option( 'page-title-space' );
            $page_title_color        = wiz_get_option( 'page-title-color' );
            $page_title_font_size        = wiz_get_option( 'page-title-font-size' );
            $page_title_font_family        = wiz_get_option( 'page-title-font-family' );
            $page_title_font_weight        = wiz_get_option( 'page-title-font-weight' );
            $page_title_font_transform        = wiz_get_option( 'pagetitle-text-transform' );
            $page_title_line_height        = wiz_get_option( 'pagetitle-line-height' );
            $Page_title_bottomline_height         = wiz_get_option( 'pagetitle-bottomline-height' );
            $Page_title_bottomline_color         = wiz_get_option( 'pagetitle-bottomline-color' );
            $Page_title_bottomline_width         = wiz_get_option( 'pagetitle-bottomline-width' );
            $layout3_border_right_color         =  wiz_get_option( 'page-title-border-right-color' );
            // Breadcrumbs
            $breadcrumbs_spacing              = wiz_get_option( 'breadcrumbs-space' );
            $breadcrumbs_color        = wiz_get_option( 'breadcrumbs-color' );
            $breadcrumbs_link_color        = wiz_get_option( 'breadcrumbs-link-color' );
            $breadcrumbs_link_h_color        = wiz_get_option( 'breadcrumbs-link-h-color' );
            
            $css_content = array(
               '.wiz-page-title-addon-content, .wiz-merged-header-title' => wiz_get_background_obj( $page_title_bg ),
               '.wiz-page-title-addon-content' => array(
                    'padding-top'    => wiz_responsive_spacing( $page_title_space, 'top', 'desktop' ),
                    'padding-right'  => wiz_responsive_spacing( $page_title_space, 'right', 'desktop' ),
                    'padding-bottom' => wiz_responsive_spacing( $page_title_space, 'bottom', 'desktop' ),
                    'padding-left'   => wiz_responsive_spacing( $page_title_space, 'left', 'desktop' ), 
               ),
               '.wiz-page-title'  => array(
                   'color'  => esc_attr( $page_title_color ),
                   'font-family'    => wiz_get_css_value( $page_title_font_family, 'font' ),
                    'font-weight'    => wiz_get_css_value( $page_title_font_weight, 'font' ),
                    'font-size'      => wiz_responsive_slider( $page_title_font_size, 'desktop' ),
                    'text-transform' => esc_attr( $page_title_font_transform ),
                    'line-height'     => esc_attr( $page_title_line_height),
               ),
               '.wiz-page-title::after' => array(
                   'background-color'  => esc_attr( $Page_title_bottomline_color ),
                   'height'  => wiz_get_css_value( $Page_title_bottomline_height, 'px' ),
                   'width'  => wiz_get_css_value( $Page_title_bottomline_width, 'px' ),
               ),
               '.page-title-layout-3 .wiz-page-title-wrap' => array(
                    'border-right-color'  => esc_attr( $layout3_border_right_color ),
                ),
               '.wiz-breadcrumb-trail'  => array (
                    'padding-top'    => wiz_responsive_spacing( $breadcrumbs_spacing, 'top', 'desktop' ),
                    'padding-right'  => wiz_responsive_spacing( $breadcrumbs_spacing, 'right', 'desktop' ),
                    'padding-bottom' => wiz_responsive_spacing( $breadcrumbs_spacing, 'bottom', 'desktop' ),
                    'padding-left'   => wiz_responsive_spacing( $breadcrumbs_spacing, 'left', 'desktop' ), 
               ),
               '.wiz-breadcrumb-trail span'  => array(
                   'color'  => esc_attr( $breadcrumbs_color ),
               ),
               '.wiz-breadcrumb-trail a span'  => array(
                   'color'  => esc_attr( $breadcrumbs_link_color ),
               ),
               '.wiz-breadcrumb-trail a:hover span'  => array(
                   'color'  => esc_attr( $breadcrumbs_link_h_color ),
               ),
 
            );

           $parse_css = wiz_parse_css( $css_content );
            
            $css_tablet = array(
                '.wiz-page-title-addon-content' => array(
                    'padding-top'    => wiz_responsive_spacing( $page_title_space, 'top', 'tablet' ),
                    'padding-right'  => wiz_responsive_spacing( $page_title_space, 'right', 'tablet' ),
                    'padding-bottom' => wiz_responsive_spacing( $page_title_space, 'bottom', 'tablet' ),
                    'padding-left'   => wiz_responsive_spacing( $page_title_space, 'left', 'tablet' ),              
                ),
                 '.wiz-page-title'  => array(
                    'font-size'      => wiz_responsive_slider( $page_title_font_size, 'tablet' ),
                ),
                '.wiz-breadcrumb-trail'  => array (
                    'padding-top'    => wiz_responsive_spacing( $breadcrumbs_spacing, 'top', 'tablet' ),
                    'padding-right'  => wiz_responsive_spacing( $breadcrumbs_spacing, 'right', 'tablet' ),
                    'padding-bottom' => wiz_responsive_spacing( $breadcrumbs_spacing, 'bottom', 'tablet' ),
                    'padding-left'   => wiz_responsive_spacing( $breadcrumbs_spacing, 'left', 'tablet' ), 
               ),
             );
            $parse_css .= wiz_parse_css( $css_tablet, '', '768' );
            
            $css_mobile = array(
                '.wiz-page-title-addon-content' => array(
                    'padding-top'    => wiz_responsive_spacing( $page_title_space, 'top', 'mobile' ),
                    'padding-right'  => wiz_responsive_spacing( $page_title_space, 'right', 'mobile' ),
                    'padding-bottom' => wiz_responsive_spacing( $page_title_space, 'bottom', 'mobile' ),
                    'padding-left'   => wiz_responsive_spacing( $page_title_space, 'left', 'mobile' ),              
                ),
                 '.wiz-page-title'  => array(
                    'font-size'      => wiz_responsive_slider( $page_title_font_size, 'mobile' ),
                ),
                '.wiz-breadcrumb-trail'  => array (
                    'padding-top'    => wiz_responsive_spacing( $breadcrumbs_spacing, 'top', 'mobile' ),
                    'padding-right'  => wiz_responsive_spacing( $breadcrumbs_spacing, 'right', 'mobile' ),
                    'padding-bottom' => wiz_responsive_spacing( $breadcrumbs_spacing, 'bottom', 'mobile' ),
                    'padding-left'   => wiz_responsive_spacing( $breadcrumbs_spacing, 'left', 'mobile' ), 
               ),
             );
            $parse_css .= wiz_parse_css( $css_mobile, '', '544' );
            
            return $dynamic_css . $parse_css;
}