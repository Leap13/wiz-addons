<?php

// Metabox of the PAGE
// Set a unique slug-like ID
//
//$prefix_page_opts = 'wiz_page_options';

//
// Create a Metabox Options
//
if( class_exists( 'KFW' ) ) {

$prefix_page_opts = 'wiz_page_options';

KFW::createMetabox( $prefix_page_opts, array(
  'title'        => 'Wiz Page Options',
  'post_type'    =>  array( 
                    'page', 
                    'post',
                    'product',
  ),
  //'show_restore' => true,
    'data_type'      => 'serialize'
) );

//
// Create a section
//
$fields = array(
  array(
    'id'    => 'wiz-main-header-display',
    'type'  => 'checkbox',
    'title' =>  __('Disable Primary Header', 'wiz-addons'),
    'label' => __('Disable Main Header In Current Post/Page.', 'wiz-addons'),
  ),
   array(
    'id'    => 'site-post-title',
    'type'  => 'checkbox',
    'title' =>  __('Disable Page Title', 'wiz-addons'),
    'label' => __('Disable Post/Page Title', 'wiz-addons'),
  ),
  array(
    'id'    => 'wiz-featured-img',
    'type'  => 'checkbox',
    'title' =>  __('Disable Featured Image', 'wiz-addons'),
    'label' => __('Disable Post/Page Featured Image', 'wiz-addons'),
  ),
      
  array(
    'id'    => 'wiz-footer-display',
    'type'  => 'checkbox',
    'title' =>  __('Disable Main Footer', 'wiz-addons'),
    'label' => __('Disable Post/Page Footer Widgets Area', 'wiz-addons'),
  ),
    
   array(
    'id'    => 'copyright-footer-layout',
    'type'  => 'checkbox',
    'title' =>  __('Disable Copyright Area', 'wiz-addons'),
    'label' => __('Disable Post/Page Copyright', 'wiz-addons'),
  ),
  
    array(
    'id'    => 'wiz-meta-enable-header-transparent',
    'type'  => 'select',
    'title' =>  __('Header Transparent', 'wiz-addons'),
    'options'     => array(
      'default'     => __('Default', 'wiz-addons'),
      'enabled'     => __('Enable', 'wiz-addons'),
      'disabled'     => __('Disabled', 'wiz-addons'),
    ),
  ),

  array(
    'id'          => 'site-sidebar-layout',
    'type'        => 'select',
    'title'       => __('Sidebar Layout', 'wiz-addons'),
    'placeholder' => 'Select an option',
    'default'     => '',
    'options'     => array(
      'no-sidebar'     => __('No Sidebar', 'wiz-addons'),
      'left-sidebar'     => __('Left Sidebar', 'wiz-addons'),
      'right-sidebar'     => __('Right Sidebar', 'wiz-addons'),
    ),
  ),

  array(
    'id'          => 'site-content-layout',
    'type'        => 'select',
    'title'       => __('Page Layout', 'wiz-addons'),
    'placeholder' => 'Select an option',
    'options'     => array(
      'boxed-container'            => __('Boxed Layout', 'wiz-addons'),
      'content-boxed-container'    => __('Boxed Content', 'wiz-addons'),
      'plain-container'            => __('Full Width Content', 'wiz-addons'),
      'page-builder'               => __('Stretched Content', 'wiz-addons'),
    ),
  ),
);
$options = get_option( 'wiz_framework' );
if($options['top-bar-section']){
    $top_bar = array(
      'id'    => 'wiz-top-bar-display',
      'type'  => 'checkbox',
      'title' =>  __('Disable Top Bar', 'wiz-addons'),
      'label' => __('Disable Top Bar In Current Post/Page.', 'wiz-addons'),
    );
    $fields[] = $top_bar;
}
KFW::createSection( $prefix_page_opts, array(
  'title'  => 'Overview',
  'icon'   => 'fa fa-rocket',
  'fields' => $fields
) );
}