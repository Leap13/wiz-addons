<?php

/**
 * Meta Box
 */
if ( ! class_exists( 'Wiz_Addon_Meta_Box_Helper' ) ) {

	/**
	 * Meta Box
	 */
	class Wiz_Addon_Meta_Box_Helper {

		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance;

		/**
		 * Initiator
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 * Constructor
		 */
		public function __construct() {
			add_action( 'wp', array( $this, 'meta_options_hooks' ) );
		}
         
		/**
		 * Metabox Hooks
		 */
		function meta_options_hooks() {

			if ( is_singular() ) {
				add_filter( 'wiz_primary_header_enabled', array( $this, 'primary_header' ) );
				add_filter( 'wiz_top_bar_enabled', array( $this, 'top_bar' ) );
				add_filter( 'wiz_header_class', array( $this, 'add_header_class' ) );
				add_filter( 'wiz_the_title_enabled', array( $this, 'post_title' ) );
				add_filter ( 'wiz_the_page_title_enabled', array( $this, 'post_title' ));
				add_filter( 'wiz_page_breadcrumbs_enabled', array( $this, 'breadcrumbs_display' ) );
				add_filter( 'wiz_featured_image_enabled', array( $this, 'featured_img' ) );
                add_filter( 'wiz_main_footer_disable', array($this, 'wiz_footer_display') );  
                add_filter( 'wiz_footer_copyright_layout_disable', array($this, 'wiz_copyright_display'));
              
			}
           
        }
        
        /**
		 * Transparent Header Option
		 */
        
        function add_header_class($classes, $default='') {
			
			$enable_trans_header = wiz_get_option( 'enable-transparent' );
			$meta = get_post_meta( get_the_ID(), 'wiz_page_options', true ); 
		    $trans_meta_option = (isset( $meta['wiz-meta-enable-header-transparent'] ) ) ? $meta['wiz-meta-enable-header-transparent'] : $default;

			if ( ('enabled' === $trans_meta_option && $enable_trans_header) || 'enabled' === $trans_meta_option  ) {
				
				$classes[] = 'wiz-header-transparent';
			} elseif ( 'disabled' === $trans_meta_option && $enable_trans_header ) {
				if (in_array('wiz-header-transparent', $classes)) {
                    unset( $classes[array_search('wiz-header-transparent', $classes)] );
                  }
			}
			
            return $classes;
            }
           
        

		/**
		 * Disable Primary Header
		 */
		function primary_header($defaults) {
            
			$meta = get_post_meta( get_the_ID(), 'wiz_page_options', true); 
			 
			$display_header = ( isset( $meta['wiz-main-header-display'] ) && $meta['wiz-main-header-display'] ) ? $meta['wiz-main-header-display'] : 'default';
	
				if ( '1' == $display_header ) {
					$defaults = false;
				}
				return $defaults;
		}

		/**
		 * Disable Top Bar
		 */
		function top_bar($defaults) {
            
			$meta = get_post_meta( get_the_ID(), 'wiz_page_options', true); 
				
			$display_top_bar = ( isset( $meta['wiz-top-bar-display'] ) && $meta['wiz-top-bar-display'] ) ? $meta['wiz-top-bar-display'] : 'default';

				if ( '1' == $display_top_bar ) {
					$defaults = false;
				}

				return $defaults;
		}

		/**
		 * Disable Post / Page Title
		 *
		 */
		function post_title( $defaults ) {
            $meta = get_post_meta( get_the_ID(), 'wiz_page_options', true ); 
            $title = ( isset( $meta['site-post-title'] ) && $meta['site-post-title'] ) ? $meta['site-post-title'] : 'default';

			if ( '1' == $title ) {
			$defaults = false;
			}

			return $defaults;
		}

		/**
		 * Disable Post / Page Featured Image
		 *
		 */
		function featured_img( $defaults ) {
            $meta = get_post_meta( get_the_ID(), 'wiz_page_options', true ); 
            $featured_img = ( isset( $meta['wiz-featured-img'] ) && $meta['wiz-featured-img'] ) ? $meta['wiz-featured-img'] : 'default';

			if ( '1' == $featured_img ) {
				$defaults = false;
			}

			return $defaults;
		}
        
        /**
		 * Disable Post / Page Footer Widgets
		 *
		 */
        function wiz_footer_display( $defaults ) {
            $meta = get_post_meta( get_the_ID(), 'wiz_page_options', true ); 
            $footer_display =  ( isset( $meta['wiz-footer-display'] ) && $meta['wiz-footer-display'] ) ? $meta['wiz-footer-display'] : 'default';
            
			if ( '1' == $footer_display ) {
                return;
            }

			return $defaults;
        }
        
        /**
		 * Disable Post / Page CopyRight
		 *
		 */
        function wiz_copyright_display( $defaults ) {
            $meta = get_post_meta( get_the_ID(), 'wiz_page_options', true ); 
            $copyright_display =  ( isset( $meta['copyright-footer-layout'] ) && $meta['copyright-footer-layout'] ) ? $meta['copyright-footer-layout'] : 'default';

			if ( '1' == $copyright_display ) {
				$defaults = true;
			}

			return $defaults;
        }
        
        /**
		 * Post / Page Sidebar Display
		 *
		 */       
        function single_page_layout( $defaults ) {
            $meta = get_post_meta( get_the_ID(), 'wiz_page_options', true ); 
            $sidebar_layout_meta = $meta['site-sidebar-layout'];

			if ( '1' == $sidebar_layout_meta ) {
				$defaults = false;
			}

			return $defaults;
		}
           
	}
}

/**
 * Kicking this off by calling 'get_instance()' method
 */
Wiz_Addon_Meta_Box_Helper::get_instance();