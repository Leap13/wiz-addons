<?php

/**
 * Wiz Meta Box Options
 *
 * @package Wiz Addons
 */

define( 'WIZ_METABOX_DIR', WIZ_ADDONS_DIR . 'addons/metabox/' );
define( 'WIZ_METABOX_URL', WIZ_ADDONS_URL . 'addons/metabox/' );

if ( ! class_exists( 'Wiz_Metabox)' ) ) {

	/**
	 * Meta Box Markup Initial Setup
	 *
	 * @since 1.0.0
	 */
	class Wiz_Metabox {

		/**
		 * Member Variable
		 *
		 * @var object instance
		 */
		private static $instance;

		/**
		 *  Initiator
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 *  Constructor
		 */
		
		public function __construct() {
			require_once WIZ_METABOX_DIR . 'classes/class-metabox-data.php';	
			require_once WIZ_METABOX_DIR . 'classes/class-metabox-data-helper.php';	
		}		

	}
    Wiz_Metabox::get_instance();
}

/**
*  Kicking this off by calling 'get_instance()' method
*/
