<?php
/**
 * Sticky Header Settings
 * 
 * @package Wiz Addons
 */
if (! class_exists('Wiz_Sticky_Header_Settings')) {

    class Wiz_Sticky_Header_Settings {

        private static $instance;

        /**
         * Initiator
         */
        
        public static function get_instance()
        {
            if (! isset(self::$instance)) {
                self::$instance = new self();
            }
            return self::$instance;
        }
        /**
		 *  Constructor
		 */
		public function __construct() {

             add_filter( 'wiz_theme_defaults', array( $this, 'theme_defaults' ) );
             add_action( 'customize_register', array( $this, 'customize_register' ) );
             add_action( 'customize_preview_init', array( $this, 'preview_scripts' ), 1 );
        }



		public function customize_register( $wp_customize ) {

			// Update the Customizer Sections under Layout.
            $wp_customize->add_section(
                    new Wiz_WP_Customize_Section(
                        $wp_customize, 'section-sticky-header', array(
                                'title'    => __( 'Sticky Header', 'wiz-addons' ),
                                'panel'    => 'panel-layout',
                                'section'  => 'section-header-group',
                                'priority' => 11,
                            )
                    )
                );
            require_once WIZ_STICKY_HEADER_DIR . 'customizer/customizer-options.php';  
        }



        public function theme_defaults( $defaults ) {
            $defaults['enable-sticky']           = '1';
            $defaults['sticky-bg-obj']           = array();
            $defaults['sticky-logo-width']     = '';
            $defaults['sticky-menu-link-color']  = '';
            $defaults['sticky-menu-link-h-color']  = '';
            $defaults['sticky-submenu-bg-color']  = '';
            $defaults['sticky-submenu-link-color']  = '';
            $defaults['sticky-submenu-link-h-color']  = '';
            return $defaults;
        }

        
        public function preview_scripts() {
                if ( SCRIPT_DEBUG ) {
				wp_enqueue_script( 'wiz-sticky-header-customize-preview-js', WIZ_STICKY_HEADER_URL . 'assets/js/unminified/customizer-preview.js', array( 'customize-preview', 'wiz-customizer-preview-js' ), WIZ_ADDONS_VERSION, true);
			} else {
                wp_enqueue_script( 'wiz-sticky-header-customize-preview-js', WIZ_STICKY_HEADER_URL . 'assets/js/minified/customizer-preview.min.js', array( 'customize-preview', 'wiz-customizer-preview-js' ), WIZ_ADDONS_VERSION, true);			}
        }


    }
}
Wiz_Sticky_Header_Settings::get_instance();
