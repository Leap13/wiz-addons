<?php
/**
 * Sticky Header - Dynamic CSS
 * 
 * @package Wiz Addons
 */

add_filter( 'wiz_dynamic_css', 'wiz_sticky_header_dynamic_css');

/**
 * Dynamic CSS
 *
 * @param  string $dynamic_css
 * @return string
 */
function wiz_sticky_header_dynamic_css( $dynamic_css ) {

			$sticky_bg_obj                    = wiz_get_option( 'sticky-bg-obj' );
			$sticky_logo_width                = wiz_get_option( 'sticky-logo-width' );
			$sticky_menu_link_color           = wiz_get_option('sticky-menu-link-color');
			$sticky_menu_link_h_color           = wiz_get_option('sticky-menu-link-h-color');
			$sticky_submenu_bg_color             = wiz_get_option( 'sticky-submenu-bg-color' );
			$sticky_submenu_link_color             = wiz_get_option( 'sticky-submenu-link-color' );
			$sticky_submenu_link_h_color             = wiz_get_option( 'sticky-submenu-link-h-color' ); 
			$sticky_border_bottom_color 	  = wiz_get_option('sticky-border-bottom-color');	   
            $css_output = array(
            //Sticky Header
				'.wiz-is-sticky .main-header-bar-wrap' => wiz_get_background_obj( $sticky_bg_obj ),
				'.wiz-is-sticky .main-header-menu a' => array(
					'color' => esc_attr($sticky_menu_link_color),
				),
				'.wiz-is-sticky .main-header-menu li:hover a,.wiz-is-sticky .main-header-menu li.current_page_item a' => array(
					'color' => esc_attr($sticky_menu_link_h_color),
				),
				'.wiz-is-sticky .main-header-menu .sub-menu li a' => array(
					'color'               => esc_attr($sticky_submenu_link_color),
					'border-bottom-color' => esc_attr( $sticky_submenu_link_color ),
				),
				'.wiz-is-sticky .main-header-menu .sub-menu li:hover > a' => array(
					'color' => esc_attr($sticky_submenu_link_h_color)
				),
				'.wiz-is-sticky .main-header-menu ul.sub-menu' => array(
					'background-color' => esc_attr( $sticky_submenu_bg_color),
				),
				'#sitehead .site-logo-img .custom-logo-link.sticky-custom-logo img' => array(
					'max-width' => wiz_responsive_slider( $sticky_logo_width, 'desktop' ),
				),
				'.wiz-is-sticky .main-header-bar' => array(
					'border-bottom-color' => esc_attr( $sticky_border_bottom_color),
				),
			);

			$parse_css = wiz_parse_css( $css_output );

			$css_tablet = array(
				'#sitehead .site-logo-img .custom-logo-link.sticky-custom-logo img' => array(
					'max-width' => wiz_responsive_slider( $sticky_logo_width, 'tablet' ),
				),
			 );
			$parse_css .= wiz_parse_css( $css_tablet, '', '768' );
			
			$css_mobile = array(
				'#sitehead .site-logo-img .custom-logo-link.sticky-custom-logo img' => array(
					'max-width' => wiz_responsive_slider( $sticky_logo_width, 'mobile' ),
				),
			);

			$parse_css .= wiz_parse_css( $css_mobile, '', '544' );
           
            
           return $dynamic_css . $parse_css;
}