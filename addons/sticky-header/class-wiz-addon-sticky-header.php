<?php

/**
 * Wiz Sticky Header Addon
 *
 * @package Wiz Addons
 */

define( 'WIZ_STICKY_HEADER_DIR', WIZ_ADDONS_DIR . 'addons/sticky-header/' );
define( 'WIZ_STICKY_HEADER_URL', WIZ_ADDONS_URL . 'addons/sticky-header/' );

if ( ! class_exists( 'Wiz_Sticky_Header' ) ) {

	class Wiz_Sticky_Header {

		private static $instance;

		/**
		 *  Initiator
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 *  Constructor
		 */
		
		public function __construct() {
            
			require_once WIZ_STICKY_HEADER_DIR . 'classes/class-sticky-header-settings.php';
			require_once WIZ_STICKY_HEADER_DIR . 'classes/class-sticky-header-partials.php';
            
            if ( ! is_admin() ) {
				require_once WIZ_STICKY_HEADER_DIR . 'classes/dynamic.css.php';
			}
		}
       
		

	}
    Wiz_Sticky_Header::get_instance();
}
