(function ($) {
    wiz_css('wiz-settings[sticky-menu-link-color]', 'color', '.wiz-is-sticky .main-header-menu a');
    wiz_css('wiz-settings[sticky-menu-link-h-color]', 'color', '.wiz-is-sticky .main-header-menu li:hover a,.wiz-is-sticky .main-header-menu li.current_page_item a');
    wiz_css('wiz-settings[sticky-submenu-bg-color]', 'background-color', '.wiz-is-sticky .main-header-menu ul.sub-menu');
    wiz_css('wiz-settings[sticky-border-bottom-color]', 'border-bottom-color', '.wiz-is-sticky .main-header-bar');
    /**
	 * Sticky Header background
	 */
	wp.customize( 'wiz-settings[sticky-bg-obj]', function( value ) {
		value.bind( function( bg_obj ) {
			var dynamicStyle = ' .wiz-is-sticky .main-header-bar-wrap { {{css}} }';	
			wiz_background_obj_css( wp.customize, bg_obj, 'sticky-bg-obj', dynamicStyle );
		} );
	} );
	wp.customize('wiz-settings[sticky-submenu-link-color]', function (setting) {
		setting.bind(function (color) {

			dynamicStyle = '.wiz-is-sticky .main-header-menu .sub-menu li a{ color: ' + color + ' } ';
			dynamicStyle += '.wiz-is-sticky .main-header-menu .sub-menu li a{ border-bottom-color: ' + color + ' } ';
			wiz_add_dynamic_css('sticky-submenu-link-color', dynamicStyle);

		});
	});
	wp.customize('wiz-settings[sticky-submenu-link-h-color]', function (setting) {
		setting.bind(function (color) {
			dynamicStyle = '.site-header.wiz-is-sticky .main-header-menu .sub-menu li:hover a,.wiz-is-sticky .main-header-menu .sub-menu li.current_page_item a{ color: ' + color + ' } ';
			wiz_add_dynamic_css('sticky-submenu-link-h-color', dynamicStyle);

		});
	});
    /*
	 * Site Identity Logo Width
	 */
	wiz_responsive_slider( 'wiz-settings[sticky-logo-width]', '#sitehead .site-logo-img .custom-logo-link.sticky-custom-logo img , wiz-is-sticky .wiz-logo-svg' , 'width');
})(jQuery);