var Header = document.querySelector('.wiz-sticky-header');

if (Header != null) {
    var sticky = Header.offsetHeight;
    window.onscroll = function () {
        if (window.pageYOffset > sticky) {
            Header.classList.add("wiz-is-sticky")
        } else {
            Header.classList.remove("wiz-is-sticky", 'swing' );
        }
    }
}