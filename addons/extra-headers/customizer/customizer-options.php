<?php

    $wp_customize->add_setting(
 		WIZ_THEME_SETTINGS . '[header-layouts]', array(
 			'default'           => wiz_get_option( 'header-layouts' ),
 			'type'              => 'option',
 			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
 		)
 	);

	$wp_customize->add_control(
		new Wiz_Control_Radio_Image(
			$wp_customize, WIZ_THEME_SETTINGS . '[header-layouts]', array(
				'section'  => 'section-header',
				'priority' => 1,
				'label'    => __( 'Wiz Headers', 'wiz-addons' ),
				'type'     => 'wiz-radio-image',
				'choices'  => array(
					'header-main-layout-1' => array(
						'label' => __( 'Logo Left', 'wiz-addons' ),
						'path'  => WIZ_EXTRA_HEADERS_URL . '/assets/images/header-layout-01.png',
					),
					'header-main-layout-2' => array(
						'label' => __( 'Logo Center', 'wiz-addons' ),
						'path'  => WIZ_EXTRA_HEADERS_URL . '/assets/images/header-layout-02.png',
					),
					'header-main-layout-3' => array(
						'label' => __( 'Logo Right', 'wiz-addons' ),
						'path'  => WIZ_EXTRA_HEADERS_URL . '/assets/images/header-layout-03.png',
					), 
                    'header-main-layout-4' => array(
						'label' => __( 'Logo Right', 'wiz-addons' ),
						'path'  => WIZ_EXTRA_HEADERS_URL . '/assets/images/header-layout-04.png',
					), 
                    'header-main-layout-5' => array(
						'label' => __( 'Logo Right', 'wiz-addons' ),
						'path'  =>  WIZ_EXTRA_HEADERS_URL . '/assets/images/header-layout-05.png',
					), 
                    'header-main-layout-6' => array(
						'label' => __( 'Logo Right', 'wiz-addons' ),
						'path'  => WIZ_EXTRA_HEADERS_URL . '/assets/images/header-layout-06.png',
					),
					'header-main-layout-7' => array(
						'label' => __( 'Logo Right', 'wiz-addons' ),
						'path'  => WIZ_EXTRA_HEADERS_URL . '/assets/images/header-layout-07.png',
					),
					'header-main-layout-8' => array(
						'label' => __( 'Logo Right', 'wiz-addons' ),
						'path'  => WIZ_EXTRA_HEADERS_URL . '/assets/images/header-layout-08.png',
					),
					'header-main-layout-9' => array(
						'label' => __( 'Logo Right', 'wiz-addons' ),
						'path'  => WIZ_EXTRA_HEADERS_URL . '/assets/images/header-layout-05.png',
					),
				),
			)
		)
	);
    /**
	 * Option: Icon Label
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[header-icon-label]', array(
			'default'           => wiz_get_option( 'header-icon-label' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[header-icon-label]', array(
			'section'  => 'section-header',
			'priority' => 10,
			'label'    => __( 'Icon Label', 'wiz' ),
			'type'     => 'text',
			'active_callback' => 'wiz_header_has_icon_label',
		)
	);
    /**
   	* Option: Icon Background Color
    */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[header-icon-bars-logo-bg-color]', array(
		  'default'           => '',
		  'type'              => 'option',
		  'transport'         => 'postMessage',
		  'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
		  $wp_customize, WIZ_THEME_SETTINGS . '[header-icon-bars-logo-bg-color]', array(
			'label'   => __( 'Logo & Menu Icon Background', 'wiz-addons' ),
			'section' => 'section-header',
			'priority' => 11,
			'active_callback' => 'has_menu_icon_bg_color',
		  )
		)
	);
    
    $wp_customize->selective_refresh->add_partial( "header-icon-bars-logo-bg-color", [
        'selector'            => ".main-header-container.logo-menu-icon",
        'settings'            => [
            "header-icon-bars-logo-bg-color",
        ],
        'render_callback'     => 'panel-layout',
        'container_inclusive' => true,
    ] );
    


   /**
   	* Option: Icon Bars Color
    */
  	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[header-icon-bars-color]', array(
		  'default'           => '#fff',
		  'type'              => 'option',
		  'transport'         => 'postMessage',
		  'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
		  $wp_customize, WIZ_THEME_SETTINGS . '[header-icon-bars-color]', array(
			'label'   => __( 'Icon Color', 'wiz-addons' ),
			'section' => 'section-header',
			'priority' => 11,
            'active_callback' => 'wiz_header_withicon_layout_style',
		  )
		)
	);
   /**
   	* Option: Icon Hover Color
    */
  	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[header-icon-bars-h-color]', array(
		  'default'           => '',
		  'type'              => 'option',
		  'transport'         => 'postMessage',
		  'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
		  $wp_customize, WIZ_THEME_SETTINGS . '[header-icon-bars-h-color]', array(
			'label'   => __( 'Icon Hover Color', 'wiz-addons' ),
			'section' => 'section-header',
			'priority' => 13,
            'active_callback' => 'wiz_header_withicon_layout_style',
		  )
		)
	);

   /**
   	* Option: Icon Background Color
    */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[header-icon-bars-bg-color]', array(
		  'default'           => '#a7a7a7',
		  'type'              => 'option',
		  'transport'         => 'postMessage',
		  'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
		  $wp_customize, WIZ_THEME_SETTINGS . '[header-icon-bars-bg-color]', array(
			'label'   => __( 'Icon Background Color', 'wiz-addons' ),
			'section' => 'section-header',
			'priority' => 14,
            'active_callback' => 'wiz_header_withicon_layout_style',
		  )
		)
	);

	/**
   	* Option: Icon Background Hover Color
    */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[header-icon-bars-bg-h-color]', array(
		  'default'           => '',
		  'type'              => 'option',
		  'transport'         => 'postMessage',
		  'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
		  $wp_customize, WIZ_THEME_SETTINGS . '[header-icon-bars-bg-h-color]', array(
			'label'   => __( 'Icon Background Hover Color', 'wiz-addons' ),
			'section' => 'section-header',
			'priority' => 15,
            'active_callback' => 'wiz_header_withicon_layout_style',
		  )
		)
	);

	/**
	 * Option: Icon Border Radius
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[header-icon-bars-border-radius]', array(
			'default'           => wiz_get_option( 'readmore-border-radius' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Slider(
			$wp_customize, WIZ_THEME_SETTINGS . '[header-icon-bars-border-radius]', array(
				'type'        => 'wiz-slider',
				'section'     => 'section-header',
				'priority'    => 16,
				'label'       => __( 'Icon Border Radius', 'wiz' ),
				'suffix'      => '',
				'input_attrs' => array(
					'min'  => 0,
					'step' => 1,
					'max'  => 100,
				),
				'active_callback' => 'wiz_header_withicon_layout_style',
			)
		)
	);
    
    /**
    * Option - Menu Icon Spacing
    */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[menu-icon-bars-space]', array(
			'default'           => '',
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_spacing' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Responsive_Spacing(
			$wp_customize, WIZ_THEME_SETTINGS . '[menu-icon-bars-space]', array(
				'type'           => 'wiz-responsive-spacing',
				'section'        => 'section-header',
				'priority'       => 10,
				'label'          => __( 'Menu Icon Space', 'wiz-addons' ),
				'linked_choices' => true,
                'active_callback' => 'wiz_header_withicon_layout_style',
				'unit_choices'   => array( 'px', 'em', '%' ),
				'choices'        => array(
					'top'    => __( 'Top', 'wiz-addons' ),
					'right'  => __( 'Right', 'wiz-addons' ),
					'bottom' => __( 'Bottom', 'wiz-addons' ),
					'left'   => __( 'Left', 'wiz-addons' ),
				),
			)
		)
	);

	/**
	 * Option: Vertical Header Position
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[v-headers-position]', array(
			'default'           => 'left',
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[v-headers-position]', array(
			'type'     => 'select',
			'section'  => 'section-header',
			'priority' => 17,
			'label'    => __( 'Header Position', 'wiz-addons' ),
			'choices'  => array(
				'left'    => __( 'Left', 'wiz-addons' ),
				'right'   => __( 'Right', 'wiz-addons' ),
			),
            'active_callback' => 'wiz_header_layout_vertical_style',
		)
	);	
    
	/**
	 * Option: Enter Width
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[vertical-header-width]', array(
			'default'           => 300,
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Slider(
			$wp_customize, WIZ_THEME_SETTINGS . '[vertical-header-width]', array(
				'type'        => 'wiz-slider',
				'section'     => 'section-header',
				'priority'    => 18,
				'label'       => __( 'Vertical Header Width', 'wiz-addons' ),
				'suffix'      => '',
				'input_attrs' => array(
					'min'  => 100,
					'step' => 1,
					'max'  => 400,
				),
                'active_callback' => 'wiz_header_layout6_style',
			)
		)
	);

	/**
	 * Option: Vertical Headers Enable Box Shadow
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[vheader-box-shadow]', array(
			'default'           => false,
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_checkbox' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[vheader-box-shadow]', array(
			'type'            => 'checkbox',
			'section'         => 'section-header',
			'label'           => __( 'Enable Box Shadow', 'wiz-addons' ),
            'priority'        => 19,
            'active_callback' => 'wiz_header_layout_vertical_style',
		)
	);
	/**
	 * Option: Vertical Headers Border Style
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[vheader-border-style]', array(
			'default'           => 'solid',
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[vheader-border-style]', array(
			'type'     => 'select',
			'section'  => 'section-header',
			'priority' => 100,
			'label'    => __( 'Border Type', 'wiz-addons' ),
			'choices'  => array(
				'hidden'    => __( 'Hidden', 'wiz-addons' ),
				'dotted'    => __( 'Dotted', 'wiz-addons' ),
				'dashed'    => __( 'Dashed', 'wiz-addons' ),
				'solid'     => __( 'Solid', 'wiz-addons' ),
				'double'    => __( 'Double', 'wiz-addons' ),
				'groove'    => __( 'Groove', 'wiz-addons' ),
				'ridge'     => __( 'Ridge', 'wiz-addons' ),
				'inset'     => __( 'Inset', 'wiz-addons' ),
				'outset'    => __( 'Outset', 'wiz-addons' ),
			),
            'active_callback' => 'wiz_header_layout_vertical_style',
		)
	);	

	/**
	 * Option: Enter Width
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[mini-vheader-width]', array(
			'default'           => 60,
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Slider(
			$wp_customize, WIZ_THEME_SETTINGS . '[mini-vheader-width]', array(
				'type'        => 'wiz-slider',
				'section'     => 'section-header',
				'priority'    => 18,
				'label'       => __( 'Vertical Header Width', 'wiz-addons' ),
				'suffix'      => '',
				'input_attrs' => array(
					'min'  => 60,
					'step' => 1,
					'max'  => 100,
				),
                'active_callback' => 'wiz_header_layout8_style',
			)
		)
	);