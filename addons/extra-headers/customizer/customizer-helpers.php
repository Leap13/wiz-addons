<?php

/* 
 * To change the Dependents options for main options
 */

function wiz_header_withicon_layout_style() {
    
	if ( 'header-main-layout-5' == wiz_get_option( 'header-layouts', 'wide' )  || 'header-main-layout-7' == wiz_get_option( 'header-layouts', 'wide' ) || 'header-main-layout-8' == wiz_get_option( 'header-layouts', 'wide' )) {
		return true;
	} else {
		return false;
	}
}
function wiz_header_has_icon_label(){
	if ( 'header-main-layout-5' == wiz_get_option( 'header-layouts', 'wide' )  || 'header-main-layout-7' == wiz_get_option( 'header-layouts', 'wide' ) ) {
		return true;
	} else {
		return false;
	}
}
function wiz_header_layout6_style() {
    
	if ( 'header-main-layout-6' == wiz_get_option( 'header-layouts', 'wide' ) ) {
		return true;
	} else {
		return false;
	}
}

function wiz_header_layout8_style() {
    
	if ( 'header-main-layout-8' == wiz_get_option( 'header-layouts', 'wide' ) ) {
		return true;
	} else {
		return false;
	}
}
function has_menu_icon_bg_color(){
	if ( 'header-main-layout-5' == wiz_get_option( 'header-layouts', 'wide' )) {
		return true;
	} else {
		return false;
	}
}
function wiz_header_layout_vertical_style() {
    
	if ( 'header-main-layout-8' == wiz_get_option( 'header-layouts', 'wide' ) || 'header-main-layout-6' == wiz_get_option( 'header-layouts', 'wide' )) {
		return true;
	} else {
		return false;
	}
}