<?php
/* 
 * Header 7 Layout
 */
$icon_label = trim( apply_filters( 'icon_header_label', wiz_get_option( 'header-icon-label' ) ) );
?>
<div class="wiz-container">
    <div class="header-layout-7-logo logo-menu-icon">
        <?php wiz_site_branding_markup(); ?>
        <?php wiz_toggle_buttons_markup(); ?>
        <?php echo wiz_header_custom_item_outside_menu(); ?>
    </div>
    <div class="menu-icon-social">
        <div class="menu-icon">
            <a id="nav-icon" class="icon-bars-btn">
                <span></span>
                <span></span>
                <span></span>
            </a>
            <?php if(!empty($icon_label)){ ?>
                <span class="header-icon-label"><?php echo esc_html( $icon_label ); ?></span>
            <?php } ?>    
        </div>
    </div>

</div>

<div class="main-header-bar-wrap ss-wrapper">
	<div class="main-header-bar ss-content">
		<?php wiz_main_header_bar_top(); ?>
        <div id="header-layout-7" class="header">
                <div class="wiz-flex main-header-container">
                    <?php wiz_primary_navigation_markup(); ?>
                </div>
		<?php wiz_main_header_bar_bottom(); ?>
	</div> 
</div>