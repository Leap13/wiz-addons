<?php
/* 
 * Header 8 Layout
 */
?>

<div class="main-header-bar-wrap ss-wrapper">
    <div class="menu-icon-social">
        <div class="menu-icon">
            <a id="nav-icon" class="icon-bars-btn">
                <span></span>
                <span></span>
                <span></span>
            </a>
        </div>
    </div>
	<div class="main-header-bar ss-content">
		<?php wiz_main_header_bar_top(); ?>
        <div id="header-layout-8" class="header">
                <div class="wiz-flex main-header-container">
                    <?php wiz_sitehead_content(); ?>
                </div>
		<?php wiz_main_header_bar_bottom(); ?>
	</div> 
</div>