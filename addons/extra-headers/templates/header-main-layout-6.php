<?php
/* 
 * Header 6 Layout
 */
?>

<div class="main-header-bar-wrap ss-wrapper">
	<div class="main-header-bar ss-content">
		<?php wiz_main_header_bar_top(); ?>
        <div id="header-layout-6" class="header">
                <div class="wiz-flex main-header-container">
                    <?php wiz_sitehead_content(); ?>
                </div>
        </div>
		<?php wiz_main_header_bar_bottom(); ?>
	</div> 
</div>
