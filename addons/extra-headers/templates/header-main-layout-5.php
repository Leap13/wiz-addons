  
<?php
/* 
 * Header 5 Layout
 */
$icon_label = trim( apply_filters( 'icon_header_label', wiz_get_option( 'header-icon-label' ) ) );
?>
<div class="main-header-bar-wrap">
	<div class="main-header-bar">
        <?php wiz_main_header_bar_top(); ?>
        <div class="header-logo-menu-icon">
            <?php if(wp_is_mobile()){ ?>
                <div class="main-header-container logo-menu-icon">
                    <div class="wiz-container">   
                    <?php wiz_site_branding_markup(); ?> 
                        <div class="menu-icon-social">
                            <div class="menu-icon">
                                <a id="nav-icon" class="icon-bars-btn">
                                  <span></span>
                                  <span></span>
                                  <span></span>
                                </a>
                                <?php if(!empty($icon_label)){ ?>
                                    <span class="header-icon-label"><?php echo esc_html( $icon_label ); ?></span>
                                <?php } ?> 
                            </div>
                            <div class="social-icons">
                                <?php echo wiz_header_custom_item_outside_menu(); ?>
                            </div>
                        </div>
                    </div>
                    <?php wiz_toggle_buttons_markup(); ?>
                </div>
                <div class="wiz-navbar-collapse">
                    <?php wiz_primary_navigation_markup(); ?>
                </div>
                <?php }else{ ?>
                    <div class="wiz-navbar-collapse">
                    <?php wiz_primary_navigation_markup(); ?>
                    </div>
                    <div class="main-header-container logo-menu-icon">
                    <div class="wiz-container">   
                    <?php wiz_site_branding_markup(); ?> 
                        <div class="menu-icon-social">
                            <div class="menu-icon">
                                <a id="nav-icon" class="icon-bars-btn">
                                  <span></span>
                                  <span></span>
                                  <span></span>
                                </a>
                                <?php if(!empty($icon_label)){ ?>
                                    <span class="header-icon-label"><?php echo esc_html( $icon_label ); ?></span>
                                <?php } ?> 
                            </div>
                            <div class="social-icons">
                                <?php echo wiz_header_custom_item_outside_menu(); ?>
                            </div>
                        </div>
                    </div>
                    <?php wiz_toggle_buttons_markup(); ?>
                </div>
                <?php } ?>
        </div><!-- Header Layout 5 -->
        <?php wiz_main_header_bar_bottom(); ?>
    </div> 
</div> 