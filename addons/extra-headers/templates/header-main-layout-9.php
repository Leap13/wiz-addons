<?php
/* 
 * Header 9 Layout
 */

$icon_label = trim( apply_filters( 'icon_header_label', wiz_get_option( 'header-icon-label' ) ) );
?>

<div class="main-header-bar-wrap"> 
    <div class="main-header-bar">
        <?php wiz_main_header_bar_top(); ?>
    
        <div class="wiz-container">
            <div id="header-layout-9" class="header inline-icon-menu-header">
                <div class="inline-logo-menu"> 
                    <?php wiz_site_branding_markup(); ?>
                    <?php wiz_toggle_buttons_markup(); ?>
                        <div class="menu-icon-social">
                            <div class="menu-icon">
                                <a id="nav-icon" class="icon-bars-btn">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </a>
                            <?php if(!empty($icon_label)){ ?>
                                <span class="header-icon-label"><?php echo esc_html( $icon_label ); ?></span>
                            <?php } ?>    
                            </div>
                        </div>
                    </div>
                    <div class="wiz-flex main-header-container">
                        <?php wiz_primary_navigation_markup(); ?>
                    </div>
                    <?php echo wiz_header_custom_item_outside_menu(); ?> 
             <?php wiz_main_header_bar_bottom(); ?>
            </div> 
        </div> 
    </div> 
</div>
