<?php
/**
 * Extra Headers
 * 
 * @package Wiz Addons
 */

if ( !class_exists( 'Wiz_Extra_Headers_Partials' )) {
    /**
	 * Extra Headers Settings
	 *
	 * @since 1.0.0
	 */
    class Wiz_Extra_Headers_Partials {
        
        private static $instance;
        
        /**
		 *  Initiator
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}
        /**
		 *  Constructor
		 */
		public function __construct() {
            
			add_filter( 'body_class', array( $this,'wiz_body_classes' ));
			add_action( 'wiz_sitehead', array( $this, 'sitehead_markup_loader'), 1);
			add_action( 'wiz_get_css_files', array( $this, 'add_styles' ) );
            add_action( 'wiz_get_js_files', array( $this, 'add_scripts' ) );
			add_filter( 'wiz_header_class', array( $this, 'header_classes' ), 10, 1 );
			//add_action( 'customize_controls_print_scripts', array( $this, 'headers_controls_scripts' ) );
        } 
        
		/**
		 * Customizer Controls
		 *
		 * @return void
		 */
		// function headers_controls_scripts() {

		// 	$js_prefix  = '.min.js';
		// 	$css_prefix = '.min.css';
		// 	$dir        = 'minified';
		// 	if ( SCRIPT_DEBUG ) {
		// 		$js_prefix  = '.js';
		// 		$dir        = 'unminified';
		// 	}

		// 	// Customizer Core.
		// 	wp_enqueue_script( 'wiz-customizer-controls-toggle-js', WIZ_EXTRA_HEADERS_URL . 'assets/js/' . $dir . '/extra-headers-controls-toggle' . $js_prefix, array(), WIZ_ADDONS_VERSION, true );

		// }
        function html_markup_loader() {
            ?>
    
            <header itemtype="https://schema.org/WPHeader" itemscope="itemscope" id="sitehead" <?php wiz_header_classes();?> role="banner">
    
                <?php wiz_sitehead_top(); ?>
    
                <?php wiz_sitehead(); ?>
    
                <?php wiz_sitehead_bottom(); ?>
    
			</header><!-- #sitehead -->
            <?php
		}
		
       function sitehead_markup_loader() {
            
			$wiz_header_layout = wiz_get_option( 'header-layouts' );
			$options = get_option( 'wiz_framework' );

			if ( apply_filters( 'wiz_primary_header_enabled', true ) ) {
				if ( 'header-main-layout-1' !== $wiz_header_layout && 'header-main-layout-2' !== $wiz_header_layout  && 'header-main-layout-3' !== $wiz_header_layout && 'header-main-layout-4' !== $wiz_header_layout  ) {
					add_action( 'wiz_header', array( $this,'html_markup_loader'));	
					remove_action( 'wiz_sitehead', 'wiz_sitehead_primary_template' );
					wizaddons_get_template( 'extra-headers/templates/'. esc_attr( $wiz_header_layout ) . '.php' );
					
				} else if ( 1 !== ( $options['extra-headers'] ) ) {
					add_action( 'wiz_sitehead', 'wiz_sitehead_primary_template' );
					}  
			}        
		}

        function wiz_body_classes($classes) {
            $wiz_header_layout = wiz_get_option( 'header-layouts' );
			$meta = get_post_meta( get_the_ID(), 'wiz_page_options', true);

            if('header-main-layout-6' == $wiz_header_layout) {
                
                $classes[] = 'header-main-layout-6';
                $classes[] = 'wiz-main-v-header-align-'. wiz_get_option('v-headers-position') ;
			} 
			if('header-main-layout-8' == $wiz_header_layout) {
                
                $classes[] = 'header-main-layout-8';
                $classes[] = 'wiz-main-v-header-align-'. wiz_get_option('v-headers-position') ;
			}
			if ( is_singular() ) {
				if(isset($meta['wiz-main-header-display']) && $meta['wiz-main-header-display'] == '1'){
					$header_align_class = 'wiz-main-v-header-align-'. wiz_get_option('v-headers-position');
					if(in_array($header_align_class , $classes)){
						$align_header = array_search($header_align_class, $classes);
						unset($classes[$align_header]);
					}
				}
			}
            return $classes;
		}
		
        function header_classes( $classes ) {
			$meta = get_post_meta( get_the_ID(), 'wiz_page_options', true);
			$wiz_header_layout = wiz_get_option( 'header-layouts' );
			$vheader_has_box_shadow   = wiz_get_option('vheader-box-shadow');
			if('header-main-layout-8' == $wiz_header_layout || 'header-main-layout-6' == $wiz_header_layout){
				if(in_array('wiz-header-transparent' , $classes)){
					$overlay_enabled = array_search('wiz-header-transparent', $classes);
					unset($classes[$overlay_enabled]);
				}
			}
			if('header-main-layout-8' == $wiz_header_layout) {
				if ($vheader_has_box_shadow == true) {
					$classes[] = 'has-box-shadow';
				}
				$classes[] = 'v-header-align-'. wiz_get_option('v-headers-position') ;
			}
			if( 'header-main-layout-6' == $wiz_header_layout ) {

				if ($vheader_has_box_shadow == true) {
					$classes[] = 'has-box-shadow';
				}
				
				$classes[] = 'v-header-align-'. wiz_get_option('v-headers-position') ;
			}
			if ( is_singular() ) {
				if(isset($meta['wiz-main-header-display']) && $meta['wiz-main-header-display'] == '1'){
					$header_align_class = 'v-header-align-'. wiz_get_option('v-headers-position');
					if(in_array($header_align_class , $classes)){
						$align_header = array_search($header_align_class, $classes);
						unset($classes[$align_header]);
					}
				}	
			}
			return $classes;
         }
        
        
         /**
		  * Enqueues scripts and styles for the header layouts
		 */
		function add_styles() {

			Wiz_Style_Generator::wiz_add_css(WIZ_EXTRA_HEADERS_DIR.'assets/css/minified/extra-header-layouts.min.css');
			Wiz_Style_Generator::wiz_add_css(WIZ_EXTRA_HEADERS_DIR.'assets/css/minified/simple-scrollbar.min.css');
		}

		public function add_scripts() {
			 Wiz_Style_Generator::wiz_add_js(WIZ_EXTRA_HEADERS_DIR.'assets/js/minified/extra-header-layouts.min.js');
			 Wiz_Style_Generator::wiz_add_js(WIZ_EXTRA_HEADERS_DIR.'assets/js/minified/simple-scrollbar.min.js');

		}
    }
}
Wiz_Extra_Headers_Partials::get_instance();