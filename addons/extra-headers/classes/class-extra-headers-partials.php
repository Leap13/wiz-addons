<?php
/**
 * Extra Headers
 * 
 * @package Wiz Addons
 */
if (! class_exists('Wiz_Extra_Header_Partials')) {

    class Wiz_Extra_Header_Partials {

        /**
         * Member Variable
         *
         * @var object instance
         */
        private static $instance;

        /**
         * Initiator
         */
        
        public static function get_instance()
        {
            if (! isset(self::$instance)) {
                self::$instance = new self();
            }
            return self::$instance;
        }
        /**
		 *  Constructor
		 */
		public function __construct() {
            
            add_filter( 'wiz_theme_defaults', array( $this, 'theme_defaults' ) );
            add_action( 'customize_register', array( $this, 'customize_register' ) );
            add_action( 'customize_register', array( $this, 'controls_helpers' ) );
            add_action( 'customize_preview_init', array( $this, 'preview_scripts' ), 1 );

        }
        

        function theme_defaults( $defaults ) {
            $defaults['header-icon-bars-logo-bg-color']  = '';
            $defaults['header-icon-bars-color']          = '#fff';
            $defaults['header-icon-bars-h-color']        = '';
            $defaults['header-icon-bars-bg-color']       = '#7a7a7a';
            $defaults['header-icon-bars-bg-h-color']     = '';
            $defaults['header-icon-bars-border-radius']  = '';
            $defaults['menu-icon-bars-space']            = '';
            $defaults['box-shadow']                      = '';
            // Vertical Headers
            $defaults['header6-position']                = '';
            $defaults['vertical-header-width']           = 300;
            $defaults['v-headers-position']              = 'left';
            $defaults['header6-border-width']            = '';
            $defaults['header6-border-style']            = '';
            $defaults['header6-border-color']            = '';
            $defaults['header8-position']                = '';
            $defaults['header8-width']                   = '';
            return $defaults;
        }

       function customize_register($wp_customize) {
			require_once WIZ_EXTRA_HEADERS_DIR . 'customizer/customizer-options.php';  
			
        }

        public function controls_helpers() {
			require_once( WIZ_EXTRA_HEADERS_DIR .'customizer/customizer-helpers.php' );
		}
        
        function preview_scripts() {
                if ( SCRIPT_DEBUG ) {
				wp_enqueue_script( 'wiz-extra-headers-customize-preview-js', WIZ_EXTRA_HEADERS_URL . 'assets/js/unminified/customizer-preview.js', array( 'customize-preview', 'wiz-customizer-preview-js' ), WIZ_ADDONS_VERSION, true);
			} else {
                wp_enqueue_script( 'wiz-extra-headers-customize-preview-js', WIZ_EXTRA_HEADERS_URL . 'assets/js/minified/customizer-preview.min.js', array( 'customize-preview', 'wiz-customizer-preview-js' ), WIZ_ADDONS_VERSION, true);			}
        }


    }
}
Wiz_Extra_Header_Partials::get_instance();
