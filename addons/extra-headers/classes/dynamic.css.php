<?php
/**
 * Extra Headers - Dynamic CSS
 * 
 * @package Wiz Addons
 */

add_filter( 'wiz_dynamic_css', 'wiz_ext_headers_dynamic_css');

/**
 * Dynamic CSS
 *
 * @param  string $dynamic_css
 * @return string
 */
function wiz_ext_headers_dynamic_css( $dynamic_css ) {
            $header_icon_bars_logo_bg_color         = wiz_get_option( 'header-icon-bars-logo-bg-color' );
            $header_icon_bars_color         = wiz_get_option( 'header-icon-bars-color' );
            $header_icon_bars_h_color       = wiz_get_option( 'header-icon-bars-h-color' );
            $header_icon_bars_bg_color      = wiz_get_option( 'header-icon-bars-bg-color' );
            $header_icon_bars_bg_h_color    = wiz_get_option( 'header-icon-bars-bg-h-color' );
            $header_icon_bars_borderradius  =  wiz_get_option( 'header-icon-bars-border-radius' );
            $space_icon_bars                = wiz_get_option( 'menu-icon-bars-space' );

            $vertical_header_width        = wiz_get_option( 'vertical-header-width' );
            $vertical_border_width         = wiz_get_option( 'header-main-sep' );
            $vheader_border_style         = wiz_get_option( 'vheader-border-style' );
            $vheader_border_color         = wiz_get_option( 'header-main-sep-color' );
            
            $mini_vheader_width         = wiz_get_option( 'mini-vheader-width' );
            $css_content = array(     
                '.logo-menu-icon' => array(
					'background-color' => esc_attr($header_icon_bars_logo_bg_color),
                ),
                '.site-header .menu-icon-social' => array(
                    'margin-top'    => wiz_responsive_spacing( $space_icon_bars, 'top', 'desktop' ),
                    'margin-right'  => wiz_responsive_spacing( $space_icon_bars, 'right', 'desktop' ),
                    'margin-bottom' => wiz_responsive_spacing( $space_icon_bars, 'bottom', 'desktop' ),
                    'margin-left'   => wiz_responsive_spacing( $space_icon_bars, 'left', 'desktop' ),              
                ),
                '.icon-bars-btn span' => array(
					'background-color' => esc_attr($header_icon_bars_color),
                ),
                '.icon-bars-btn:hover span, .open .icon-bars-btn span' => array(
					'background-color' => esc_attr($header_icon_bars_h_color),
                ),
                '.menu-icon-social .menu-icon' => array(
                    'background-color' => esc_attr($header_icon_bars_bg_color),
                    'border-radius'    => wiz_get_css_value( $header_icon_bars_borderradius, 'px' ),
                ),
                '.menu-icon-social .menu-icon:hover, .menu-icon-social .menu-icon.open' => array(
					'background-color' => esc_attr($header_icon_bars_bg_h_color),
                ),
                '.header-main-layout-6 .main-header-bar-wrap' => array(
                    'width' => wiz_get_css_value( $vertical_header_width, 'px' ),
                    'border-color' => esc_attr( $vheader_border_color ),
                ),
                '.header-main-layout-8 .main-header-bar-wrap' => array(
                    'width' => wiz_get_css_value( $mini_vheader_width, 'px' ),
                    'border-color' => esc_attr( $vheader_border_color ),
                ),
                '.wiz-main-v-header-align-right.header-main-layout-8' => array(
                    'padding-right' => wiz_get_css_value( $mini_vheader_width , 'px'),
                ),
                '.wiz-main-v-header-align-left.header-main-layout-8' => array(
                    'padding-left' => wiz_get_css_value( $mini_vheader_width , 'px'),
                ),
                '.wiz-main-v-header-align-right .main-header-bar-wrap' => array(
                    'border-left-style' => esc_attr( $vheader_border_style ),
                    'border-left-width' => wiz_responsive_slider( $vertical_border_width , 'desktop' ),
                ),
                '.wiz-main-v-header-align-left .main-header-bar-wrap' => array(
                    'border-right-style' => esc_attr( $vheader_border_style ),
                    'border-right-width' => wiz_responsive_slider( $vertical_border_width , 'desktop' ),
                ),
                '.wiz-main-v-header-align-right.header-main-layout-6' => array(
                    'padding-right' => wiz_get_css_value( $vertical_header_width , 'px'),
                ),
                '.wiz-main-v-header-align-left.header-main-layout-6' => array(
                    'padding-left' => wiz_get_css_value( $vertical_header_width , 'px'),
                ),     
            );

            $parse_css = wiz_parse_css( $css_content );
            
            $css_tablet = array(
                '.site-header .menu-icon-social' => array(
                    'margin-top'    => wiz_responsive_spacing( $space_icon_bars, 'top', 'tablet' ),
                    'margin-right'  => wiz_responsive_spacing( $space_icon_bars, 'right', 'tablet' ),
                    'margin-bottom' => wiz_responsive_spacing( $space_icon_bars, 'bottom', 'tablet' ),
                    'margin-left'   => wiz_responsive_spacing( $space_icon_bars, 'left', 'tablet' ),              
                ),
             );
           $parse_css .= wiz_parse_css( $css_tablet, '', '768' );
            
            $css_mobile = array(
                '.site-header .menu-icon-social' => array(
                    'margin-top'    => wiz_responsive_spacing( $space_icon_bars, 'top', 'mobile' ),
                    'margin-right'  => wiz_responsive_spacing( $space_icon_bars, 'right', 'mobile' ),
                    'margin-bottom' => wiz_responsive_spacing( $space_icon_bars, 'bottom', 'mobile' ),
                    'margin-left'   => wiz_responsive_spacing( $space_icon_bars, 'left', 'mobile' ),              
                ),
             );
           $parse_css .= wiz_parse_css( $css_mobile, '', '544' );
            
            return $dynamic_css . $parse_css;
}