<?php 
/**
 * Wiz Extra Headers Addon
 *
 * @package Wiz Addons
 */

define( 'WIZ_EXTRA_HEADERS_DIR', WIZ_ADDONS_DIR . 'addons/extra-headers/' );
define( 'WIZ_EXTRA_HEADERS_URL', WIZ_ADDONS_URL . 'addons/extra-headers/' );

if ( ! class_exists( 'Wiz_Extra_Headers' ) ) {

	/**
	 * Extra Headers
	 *
	 * @since 1.0.0
	 */
	class Wiz_Extra_Headers {

		/**
		 * Member Variable
		 *
		 * @var object instance
		 */
		private static $instance;

		/**
		 *  Initiator
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 *  Constructor
		 */
		
		public function __construct() {
            
            require_once WIZ_EXTRA_HEADERS_DIR . 'classes/class-extra-headers-partials.php';
            require_once WIZ_EXTRA_HEADERS_DIR . 'classes/class-extra-headers-settings.php';
            
            if ( ! is_admin() ) {
				require_once WIZ_EXTRA_HEADERS_DIR . 'classes/dynamic.css.php';
			}
		}

	}
    Wiz_Extra_Headers::get_instance();
}

/**
*  Kicking this off by calling 'get_instance()' method
*/
