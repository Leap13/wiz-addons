/**
 * Customizer controls toggles
 *
 * @package Wiz
 */

( function( $ ) {

	/* Internal shorthand */
	var api = wp.customize;

	/**
	 * Trigger hooks
	 */
	WIZControlTrigger = {

	    /**
	     * Trigger a hook.
	     *
	     * @method triggerHook
	     * @param {String} hook The hook to trigger.
	     * @param {Array} args An array of args to pass to the hook.
		 */
	    triggerHook: function( hook, args )
	    {
	    	$( 'body' ).trigger( 'wiz-addons-control-trigger.' + hook, args );
	    },

	    /**
	     * Add a hook.
	     *
	     * @method addHook
	     * @param {String} hook The hook to add.
	     * @param {Function} callback A function to call when the hook is triggered.
	     */
	    addHook: function( hook, callback )
	    {
	    	$( 'body' ).on( 'wiz-addons-control-trigger.' + hook, callback );
	    },

	    /**
	     * Remove a hook.
	     *
	     * @method removeHook
	     * @param {String} hook The hook to remove.
	     * @param {Function} callback The callback function to remove.
	     */
	    removeHook: function( hook, callback )
	    {
		    $( 'body' ).off( 'wiz-addons-control-trigger.' + hook, callback );
	    },
	};

	/**
	 * Helper class that contains data for showing and hiding controls.
	 *
	 * @class WIZCustomizerToggles
	 */
	WIZCustomizerToggles = {
        'wiz-settings[header-layouts]':
		[
			{
				controls: [
					'wiz-settings[enable-transparent]',
				],
				callback: function (value) {

					if (value == 'header-main-layout-6' || value == 'header-main-layout-8') {
						return false;
					}
					return true;
				}
			},
			{
				controls: [
					'wiz-settings[header-icon-label]',
				],
				callback: function (value) {

					if (value == 'header-main-layout-7' || value == 'header-main-layout-5') {
						return true;
					}
					return false;
				}
			},
		],
	};
} )( jQuery );