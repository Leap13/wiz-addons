(function ($) {
    wiz_css('wiz-settings[header-icon-bars-logo-bg-color]', 'background-color', '.site-header .logo-menu-icon');
    wiz_css('wiz-settings[header-main-sep-color]', 'border-color', '.wiz-main-v-header-align-right .main-header-bar-wrap , .wiz-main-v-header-align-left .main-header-bar-wrap');
    wiz_css('wiz-settings[header-icon-bars-color]', 'background-color', '.icon-bars-btn span');
    wiz_css('wiz-settings[header-icon-bars-h-color]', 'background-color', '.icon-bars-btn:hover span, .open .icon-bars-btn span');
    wiz_css('wiz-settings[header-icon-bars-bg-color]', 'background-color', '.menu-icon-social .menu-icon');
    wiz_css('wiz-settings[header-icon-bars-bg-h-color]', 'background-color', '.menu-icon-social .menu-icon:hover, .menu-icon-social .menu-icon.open');
    wp.customize('wiz-settings[header-icon-bars-border-radius]', function (setting) {
        setting.bind(function (border) {

            var dynamicStyle = '.site-header .menu-icon-social .menu-icon { border-radius: ' + (parseInt(border)) + 'px } ';
            wiz_add_dynamic_css('header-icon-bars-border-radius', dynamicStyle);

        });
    });
    wp.customize('wiz-settings[mini-vheader-width]', function (setting) {
        setting.bind(function (width) {

            var dynamicStyle = '.header-main-layout-8 .main-header-bar-wrap { width: ' + (parseInt(width)) + 'px } .header-main-layout-8.wiz-main-v-header-align-right { padding-right: ' + (parseInt(width)) + 'px } .header-main-layout-8.wiz-main-v-header-align-left { padding-left: ' + (parseInt(width)) + 'px }';
            wiz_add_dynamic_css('mini-vheader-width', dynamicStyle);

        });
    });
    wp.customize('wiz-settings[vertical-header-width]', function (setting) {
        setting.bind(function (width) {

            var dynamicStyle = '.header-main-layout-6 .main-header-bar-wrap { width: ' + (parseInt(width)) + 'px } .header-main-layout-6.wiz-main-v-header-align-right { padding-right: ' + (parseInt(width)) + 'px } .header-main-layout-6.wiz-main-v-header-align-left { padding-left: ' + (parseInt(width)) + 'px }';
            wiz_add_dynamic_css('vertical-header-width', dynamicStyle);

        });
    });

	wp.customize('wiz-settings[header-icon-label]', function (setting) {
		setting.bind(function (label) {
            if ($('.menu-icon-social .menu-icon .header-icon-label').length > 0) {
                $('.menu-icon-social .menu-icon .header-icon-label').text(label);
            } else {
				var html = $('.menu-icon-social .menu-icon').html();
				if ('' != label) {
					html += '<span class="header-icon-label">' + label + '</span>';
				}
				$('.menu-icon-social .menu-icon').html(html)
			}
            
		});
	});
    wiz_responsive_slider('wiz-settings[header-main-sep]', '.wiz-main-v-header-align-right .main-header-bar-wrap', 'border-left-width');
    wiz_responsive_slider('wiz-settings[header-main-sep]', '.wiz-main-v-header-align-left .main-header-bar-wrap', 'border-right-width');
    wiz_responsive_spacing('wiz-settings[menu-icon-bars-space]', '.site-header .menu-icon-social', 'margin', ['top', 'right', 'bottom', 'left']);

})(jQuery);
