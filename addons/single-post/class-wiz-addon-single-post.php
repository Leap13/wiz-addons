<?php // 
/**
 * Wiz Single Post
 *
 * @package Wiz Addons
 */

define( 'WIZ_SINGLE_POST_DIR', WIZ_ADDONS_DIR . 'addons/single-post/' );
define( 'WIZ_SINGLE_POST_URL', WIZ_ADDONS_URL . 'addons/single-post/' );

if ( ! class_exists( 'Wiz_Single_Post' ) ) {

	/**
	 * Single Post
	 *
	 * @since 1.0.0
	 */
	class Wiz_Single_Post {

		/**
		 * Member Variable
		 *
		 * @var object instance
		 */
		private static $instance;

		/**
		 *  Initiator
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 *  Constructor
		 */
		
		public function __construct() {
            
            require_once WIZ_SINGLE_POST_DIR . 'classes/class-single-post-partials.php';
            require_once WIZ_SINGLE_POST_DIR . 'classes/class-single-post-settings.php';
            
            if ( ! is_admin() ) {
				require_once WIZ_SINGLE_POST_DIR . 'classes/dynamic.css.php';
			}
		}
		

	}
    Wiz_Single_Post::get_instance();
}

/**
*  Kicking this off by calling 'get_instance()' method
*/
