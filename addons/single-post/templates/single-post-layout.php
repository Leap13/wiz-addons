<?php
/**
 * Template for Single post
 *
 * @package     Wiz
 * @author      Wiz
 * @copyright   Copyright (c) 2019, Wiz
 * @link        https://wiz.io/
 * @since       Wiz 1.0.0
 */

?>

<div <?php wiz_blog_layout_class( 'single-layout-1' ); ?>>

	<?php wiz_single_header_before(); ?>

	<header class="entry-header <?php wiz_entry_header_class(); ?>">

		<?php wiz_single_header_top(); ?>

		<?php wiz_single_post_thumbnai_and_title_order(); ?>

		<?php wiz_single_header_bottom(); ?>

	</header><!-- .entry-header -->

	<?php wiz_single_header_after(); ?>

	<div class="entry-content clear" itemprop="text">

		<?php wiz_entry_content_before(); ?>

		<?php the_content(); ?>

		<?php
			wiz_edit_post_link(

				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'wiz' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>

		<?php wiz_entry_content_after(); ?>

		<?php
			wp_link_pages(
				array(
					'before'      => '<div class="page-links">' . esc_html( wiz_theme_strings( 'string-single-page-links-before', false ) ),
					'after'       => '</div>',
					'link_before' => '<span class="page-link">',
					'link_after'  => '</span>',
				)
			);
		?>
	</div><!-- .entry-content .clear -->
</div>

<?php if(wiz_get_option('enable-author-box') == true){ 
	add_action('wiz_entry_after' , 'author_box_template' , 1);

	function author_box_template(){
?>
<section class="wiz-author-box wiz-archive-description">
	<div class="wiz-author-bio">
		<h1 class='page-title wiz-archive-title'><?php echo get_the_author(); ?></h1>
		<p><?php echo wp_kses_post( get_the_author_meta( 'description' ) ); ?></p>
	</div>
	<div class="wiz-author-avatar">
		<?php echo get_avatar( get_the_author_meta( 'email' ), 120 ); ?>
	</div>
</section>

<?php }
} ?>