<?php
/**
 * Single Post Section
 * 
 * @package Wiz Addons
 */
if (! class_exists('Wiz_Single_Post_Partials')) {

    /**
     * Single Post Section
     *
     * @since 1.0.0
     */
    class Wiz_Single_Post_Partials
    {

        private static $instance;

        /**
         * Initiator
         */
        
        public static function get_instance()
        {
            if (! isset(self::$instance)) {
                self::$instance = new self();
            }
            return self::$instance;
        }
        /**
		 *  Constructor 
		 */
		public function __construct() {
            add_filter( 'body_class', array( $this,'wiz_body_classes' ));
            add_action( 'wiz_get_css_files', array( $this, 'add_styles' ) );
            add_action( 'wiz_entry_content_single', array( $this, 'wiz_single_post_template_loader') , 1);
            add_filter( 'wiz_the_title_enabled', array( $this, 'enable_page_title_in_content' ) );
        }
        public function wiz_single_post_template_loader() {
            remove_action( 'wiz_entry_content_single', 'wiz_entry_content_single_template' );
            wizaddons_get_template( 'single-post/templates/single-post-layout.php' );  
        }
        function wiz_body_classes($classes) {
            
            $prev_next_links = wiz_get_option('prev-next-links');

			if($prev_next_links == true){
				$classes[] = 'hide-nav-links';
			}
            return $classes;
        }
        
        function enable_page_title_in_content(){
            if(is_singular()){
                return wiz_get_option('enable-page-title-content-area');
            }else{
                return true;
            }       
        }
        
         /**
		  * Enqueues scripts and styles for the header layouts
		 */
		function add_styles() {
            Wiz_Style_Generator::wiz_add_css(WIZ_SINGLE_POST_DIR.'assets/css/minified/style.min.css');
		}

		// public function add_scripts() {
		// 	 Wiz_Style_Generator::wiz_add_js(WIZ_EXTRA_HEADERS_DIR.'assets/js/minified/extra-header-layouts.min.js');

		// }

    }
}
Wiz_Single_Post_Partials::get_instance();
