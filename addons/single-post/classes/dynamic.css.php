<?php
/**
 * Single Post - Dynamic CSS
 * 
 * @package Wiz Addons
 */

add_filter( 'wiz_dynamic_css', 'wiz_single_post_dynamic_css');

/**
 * Dynamic CSS
 *
 * @param  string $dynamic_css
 * @return string
 */
function wiz_single_post_dynamic_css( $dynamic_css ) {
            global $post;
            $header_featured_image = '';
            $title_meta_poistion = wiz_get_option('title-meta-position');
            $content_alignment = wiz_get_option('content-alignment');
            $padding_inside_container = wiz_get_option('padding-inside-container');
            
            if(wiz_get_option('featured-image-header') == true){
                if(has_post_thumbnail( $post->ID )){
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                    $header_featured_image = array(
                        'background-image'      => $image[0],
                        'background-repeat'     => 'no-repeat',
                        'background-position'   => 'center center',
                        'background-size'       => 'cover',
                        'background-attachment' => 'scroll',
                    );
                }
            }
            
            $css_content = array(
                '.single .entry-header' => array(
                        'text-align' => esc_attr($title_meta_poistion),
                ),
                '.wiz-page-title-addon-content, .wiz-merged-header-title' => wiz_get_background_obj( $header_featured_image ),  
                '.single-post.wiz-separate-container .wiz-article-single, .single-post .comments-area .comment-respond , .single-post .wiz-author-box' => array(
                'padding-top'    => wiz_responsive_spacing( $padding_inside_container, 'top', 'desktop' ),
                'padding-right'  => wiz_responsive_spacing( $padding_inside_container, 'right', 'desktop' ),
                'padding-bottom' => wiz_responsive_spacing( $padding_inside_container, 'bottom', 'desktop' ),
                'padding-left'   => wiz_responsive_spacing( $padding_inside_container, 'left', 'desktop' ),              
                ),
                '.single .entry-content , .single .comments-area , .single .comments-area .comment-form-textarea textarea' => array(
                    'text-align' => esc_attr($content_alignment),
                ),
            );

            $parse_css = wiz_parse_css( $css_content );
            
            return $dynamic_css . $parse_css;
}