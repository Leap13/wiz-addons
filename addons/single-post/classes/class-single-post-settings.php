<?php
/**
 * Extra Headers
 * 
 * @package Wiz Addons
 */

if ( !class_exists( 'Wiz_Single_Post_Setting' )) {
    /**
	 * Single Post Settings
	 *
	 * @since 1.0.0
	 */
    class Wiz_Single_Post_Setting {
        
        private static $instance;
        
        /**
		 *  Initiator
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}
        /**
		 *  Constructor
		 */
		public function __construct() {
            add_filter( 'wiz_theme_defaults', array( $this, 'theme_defaults' ) );
			add_action( 'customize_preview_init', array( $this, 'preview_scripts' ) );
			add_action( 'customize_register', array( $this, 'customize_register' ) );
            //add_filter( 'wiz_header_class', array( $this, 'header_classes' ), 10, 1 );
        }
        
		function theme_defaults( $defaults ) {
            
			$defaults['prev-next-links'] = '';
			$defaults['padding-inside-container'] = '';
			$defaults['featured-image-header'] = '';
			$defaults['wiz-related-posts-taxonomy'] = '';
            return $defaults;
        }
		function customize_register($wp_customize) {
			require_once WIZ_SINGLE_POST_DIR . 'customizer/customizer-options.php';  
			
        }
		function preview_scripts() {
			if ( SCRIPT_DEBUG ) {
			wp_enqueue_script( 'wiz-single-post-customize-preview-js', WIZ_SINGLE_POST_URL . 'assets/js/unminified/customizer-preview.js', array( 'customize-preview', 'wiz-customizer-preview-js' ), WIZ_ADDONS_VERSION, true);
		} else {
			wp_enqueue_script( 'wiz-single-post-customize-preview-js', WIZ_SINGLE_POST_URL . 'assets/js/minified/customizer-preview.min.js', array( 'customize-preview', 'wiz-customizer-preview-js' ), WIZ_ADDONS_VERSION, true);			}
	}
        
    }
}
Wiz_Single_Post_Setting::get_instance();