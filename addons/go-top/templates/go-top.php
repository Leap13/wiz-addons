<?php
/**
 * Template for Go Top Template
 */
$gotop_responsive = wiz_get_option( 'go-top-responsive' );
?>
    <div class="wiz-go-top-container <?php echo esc_attr( $gotop_responsive ); ?>">
        <button class="wiz-go-top-link" id="wiz-go-top">
            <span><?php esc_html_e( 'Go Top', 'wiz-addons' ); ?></span>
        </button>
    </div> 
