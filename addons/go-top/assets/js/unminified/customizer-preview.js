(function ($) {
    
    wiz_responsive_slider('wiz-settings[go-top-icon-size]', '.wiz-go-top-link:before', 'font-size');
    wiz_css('wiz-settings[go-top-icon-color]', 'color', ' .wiz-go-top-link');
    wiz_css('wiz-settings[go-top-icon-h-color]', 'color', ' .wiz-go-top-link:hover');
    wiz_css('wiz-settings[go-top-bg-color]', 'background-color', '.wiz-go-top-link');
    wiz_css('wiz-settings[go-top-bg-h-color]', 'background-color', '.wiz-go-top-link:hover');
    wiz_responsive_slider('wiz-settings[go-top-border-radius]', '.wiz-go-top-link', 'border-radius');
    wiz_responsive_slider('wiz-settings[go-top-button-size]', '.wiz-go-top-link', 'width');
    wiz_responsive_slider('wiz-settings[go-top-button-size]', '.wiz-go-top-link', 'line-height');
    wiz_responsive_slider('wiz-settings[go-top-button-size]', '.wiz-go-top-link', 'height');

})(jQuery);