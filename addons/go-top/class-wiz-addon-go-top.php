<?php
/**
 * Wiz Go Top Options
 *
 * @package Wiz Addons
 */

define( 'WIZ_GOTOP_DIR', WIZ_ADDONS_DIR . 'addons/go-top/' );
define( 'WIZ_GOTOP_URL', WIZ_ADDONS_URL . 'addons/go-top/' );

if ( ! class_exists( 'Wiz_GoTop' ) ) {

	/**
	 * Go Top Setup
	 *
	 * @since 1.0.0
	 */
	class Wiz_GoTop {

		/**
		 * Member Variable
		 *
		 * @var object instance
		 */
		private static $instance;

		/**
		 *  Initiator
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 *  Constructor
		 */
		
		public function __construct() {
            
			require_once WIZ_GOTOP_DIR . 'classes/class-go-top-settings.php';
			require_once WIZ_GOTOP_DIR . 'classes/class-go-top-partials.php';

            if ( ! is_admin() ) {
				require_once WIZ_GOTOP_DIR . 'classes/dynamic.css.php';
			}
		}

	}

    Wiz_GoTop::get_instance();
}