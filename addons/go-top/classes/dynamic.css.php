<?php
/**
 * Go Top - Dynamic CSS
 * 
 * @package Wiz Addons
 */

add_filter( 'wiz_dynamic_css', 'wiz_ext_go_top_dynamic_css');

/**
 * Dynamic CSS
 *
 * @param  string $dynamic_css
 * @return string
 */
function wiz_ext_go_top_dynamic_css( $dynamic_css ) {
			// Go Top Link
			$go_top_icon_color             = wiz_get_option('go-top-icon-color');
			$go_top_icon_h_color           = wiz_get_option('go-top-icon-h-color');
			$go_top_icon_size              = wiz_get_option('go-top-icon-size');
			$go_top_bg_color               = wiz_get_option('go-top-bg-color');
			$go_top_bg_h_color             = wiz_get_option('go-top-bg-h-color');
			$go_top_border_radius          = wiz_get_option('go-top-border-radius');
			$go_top_button_size            = wiz_get_option('go-top-button-size');
            
            $css_content = array(
                '.wiz-go-top-link' => array(
					'background-color' => esc_attr( $go_top_bg_color ),
					'border-radius'    => wiz_responsive_slider( $go_top_border_radius, 'desktop' ),
					'width'            => wiz_responsive_slider( $go_top_button_size,'desktop' ),
					'height'           => wiz_responsive_slider( $go_top_button_size,'desktop' ),
					'line-height'      => wiz_responsive_slider( $go_top_button_size,'desktop'),
					'color'            => esc_attr($go_top_icon_color),
				),
				'.wiz-go-top-link:before' => array(
					'font-size'      => wiz_responsive_slider( $go_top_icon_size, 'desktop' ),
				),
				'.wiz-go-top-link:hover' => array(
					'color'            => esc_attr($go_top_icon_h_color),
					'background-color' => esc_attr($go_top_bg_h_color)
				),
 
            );

           $parse_css = wiz_parse_css( $css_content );
            
            $css_tablet = array(
                '.wiz-go-top-link:before' => array(
                    'font-size'    => wiz_responsive_slider( $go_top_icon_size, 'tablet' ),
                ),
				'.wiz-go-top-link' => array(
					'border-radius'    => wiz_responsive_slider( $go_top_border_radius, 'tablet' ),
					'width'            => wiz_responsive_slider( $go_top_button_size,'tablet' ),
					'height'           => wiz_responsive_slider( $go_top_button_size,'tablet' ),
					'line-height'      => wiz_responsive_slider( $go_top_button_size,'tablet'),
				),
             );
            $parse_css .= wiz_parse_css( $css_tablet, '', '768' );
            
            $css_mobile = array(
                '.wiz-go-top-link:before' => array(
                    'font-size'    => wiz_responsive_slider( $go_top_icon_size, 'mobile' ),
                ),
				'.wiz-go-top-link' => array(
					'border-radius'    => wiz_responsive_slider( $go_top_border_radius, 'mobile' ),
					'width'            => wiz_responsive_slider( $go_top_button_size,'mobile' ),
					'height'           => wiz_responsive_slider( $go_top_button_size,'mobile' ),
					'line-height'      => wiz_responsive_slider( $go_top_button_size,'mobile'),
				),
             );
            $parse_css .= wiz_parse_css( $css_mobile, '', '544' );
            
            return $dynamic_css . $parse_css;
}