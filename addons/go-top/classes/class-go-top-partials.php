<?php
/**
 * Go Top Partials
 * 
 * @package Wiz Addons
 */
if (! class_exists('Wiz_Go_Top_Partials')) {

    class Wiz_Go_Top_Partials {

        private static $instance;

        /**
         * Initiator
         */
        
        public static function get_instance()
        {
            if (! isset(self::$instance)) {
                self::$instance = new self();
            }
            return self::$instance;
        }
        /**
		 *  Constructor
		 */
		public function __construct() {
            add_action( 'wiz_get_css_files', array( $this, 'add_styles' ) );
            add_action( 'wiz_get_js_files', array( $this, 'add_scripts' ) );
            add_action( 'wiz_footer', array( $this, 'wiz_go_top_markup') );
        }

        public function wiz_go_top_markup() {
            if( wiz_get_option( 'enable-go-top' ) ){
            require_once WIZ_GOTOP_DIR . 'templates/go-top.php';
            }
        }


        public function add_styles() {
            Wiz_Style_Generator::wiz_add_css(WIZ_GOTOP_DIR.'assets/css/minified/style.min.css');

        }
        
        public function add_scripts() {
			 Wiz_Style_Generator::wiz_add_js(WIZ_GOTOP_DIR.'assets/js/minified/go-top.min.js');
		}
        
    }
}
Wiz_Go_Top_Partials::get_instance();
