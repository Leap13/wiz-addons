<?php
/**
 * Go Top Settings Defaults, Customizer, Customizer Preview
 * 
 * @package Wiz Addons
 */
if (! class_exists('Wiz_Go_Top_Settings')) {

    /**
     * Go Top Section
     *
     * @since 1.0.0
     */
    class Wiz_Go_Top_Settings
    {

        private static $instance;

        /**
         * Initiator
         */
        
        public static function get_instance()
        {
            if (! isset(self::$instance)) {
                self::$instance = new self();
            }
            return self::$instance;
        }
        
        /**
		 *  Constructor
		 */
		public function __construct() {
            add_action( 'customize_register', array( $this, 'customize_register' ) );
            add_filter( 'wiz_theme_defaults', array( $this, 'theme_defaults' ) );
            add_action( 'customize_preview_init', array( $this, 'preview_scripts' ), 1 );
        }

		public function customize_register( $wp_customize ) {

			// Update the Customizer Sections under Layout.
            $wp_customize->add_section(
                new Wiz_WP_Customize_Section(
                    $wp_customize, 'section-go-top', array(
                            'title'    => __( 'Go Top Section', 'wiz-addons' ),
                            'panel'    => 'panel-layout',
                            'section'   => 'section-footer-group',
                            'priority' => 40,
                        )
                )
            );
            require_once WIZ_GOTOP_DIR . 'customizer/customizer-options.php';  
            require_once WIZ_GOTOP_DIR .'customizer/function-helper.php';
        }



        public function theme_defaults( $defaults ) {
            $defaults['enable-go-top']           = '1';
            $defaults['go-top-button-size']           = '';
            $defaults['go-top-icon-size']           = '';
            $defaults['go-top-border-radius']           = '';
            $defaults['go-top-icon-color']           = '';
            $defaults['go-top-icon-h-color']           = '';
            $defaults['go-top-bg-color']           = '';
            $defaults['go-top-bg-h-color']           = '';
            $defaults['go-top-responsive']           = 'all-devices';

            return $defaults;
        }
        
        public function preview_scripts() {
                if ( SCRIPT_DEBUG ) {
				wp_enqueue_script( 'wiz-go-top-customize-preview-js', WIZ_GOTOP_URL . 'assets/js/unminified/customizer-preview.js', array( 'customize-preview', 'wiz-customizer-preview-js' ), WIZ_ADDONS_VERSION, true);
			} else {
                wp_enqueue_script( 'wiz-go-top-customize-preview-js', WIZ_GOTOP_URL . 'assets/js/minified/customizer-preview.min.js', array( 'customize-preview', 'wiz-customizer-preview-js' ), WIZ_ADDONS_VERSION, true);			
            }
        }
    }
}
Wiz_Go_Top_Settings::get_instance();
