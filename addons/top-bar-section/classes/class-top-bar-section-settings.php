<?php
/**
 * Top Bar Section
 * 
 * @package Wiz Addons
 */
if (! class_exists('Wiz_Top_Bar_Settings')) {


    class Wiz_Top_Bar_Settings {

        private static $instance;

        /**
         * Initiator
         */
        
        public static function get_instance()
        {
            if (! isset(self::$instance)) {
                self::$instance = new self();
            }
            return self::$instance;
        }
        /**
		 *  Constructor
		 */
		public function __construct() {
            add_action( 'customize_register', array( $this, 'customize_register' ) );
            add_action( 'customize_register', array( $this, 'controls_helpers' ) );
            add_filter( 'wiz_theme_defaults', array( $this, 'theme_defaults' ) );
            add_action( 'customize_preview_init', array( $this, 'preview_scripts' ), 1 );
        }

        public function controls_helpers() {
			require_once( WIZ_TOPBAR_DIR .'customizer/customizer-helpers.php' );
		}
		public function customize_register( $wp_customize ) {

			// Update the Customizer Sections under Layout.
            $wp_customize->add_section(
                new Wiz_WP_Customize_Section(
                    $wp_customize, 'section-topbar-header', array(
                            'title'    => __( 'Top Bar Section', 'wiz-addons' ),
                            'panel'    => 'panel-layout',
                            'section'  => 'section-header-group',
                            'priority' => 15,
                        )
                )
            );
            require_once WIZ_TOPBAR_DIR . 'customizer/customizer-options.php';  
        }

        
        function theme_defaults( $defaults ) {
            $defaults['top-section-1-html']              = '';
            $defaults['top-section-2-html']                    = '';
            $defaults['topbar-padding']         = '';
            $defaults['topbar-responsive']         = 'all-devices';
            $defaults['topbar-bg-color']    = '';
            $defaults['topbar-font-size']                  = '';
            $defaults['topbar-text-color']                  = '';
            $defaults['top-bar-search-style']               = 'search-box';
            $defaults['topbar-bg-color']             = '';
            $defaults['topbar-link-color']           = '';
            $defaults['topbar-link-h-color']           = '';
            $defaults['topbar-border-size']           = '';
            $defaults['topbar-border-bottom-color']           = '';
            $defaults['topbar-submenu-bg-color']           = '';
            $defaults['topbar-submenu-items-color']           = '';
            $defaults['topbar-submenu-items-h-color']           = '';

            return $defaults;
        }
        
        function preview_scripts() {
                if ( SCRIPT_DEBUG ) {
				wp_enqueue_script( 'wiz-topbar-customize-preview-js', WIZ_TOPBAR_URL . 'assets/js/unminified/customizer-preview.js', array( 'customize-preview', 'wiz-customizer-preview-js' ), WIZ_ADDONS_VERSION, true);
			} else {
                wp_enqueue_script( 'wiz-topbar-customize-preview-js', WIZ_TOPBAR_URL . 'assets/js/minified/customizer-preview.min.js', array( 'customize-preview', 'wiz-customizer-preview-js' ), WIZ_ADDONS_VERSION, true);			}
        }

    }
}
Wiz_Top_Bar_Settings::get_instance();
