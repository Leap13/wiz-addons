<?php
/**
 * Top Bar Section - Dynamic CSS
 *
 * @package Wiz Addons
 */

add_filter( 'wiz_dynamic_css', 'wiz_topbar_dynamic_css');

/**
 * Dynamic CSS
 *
 * @param  string $dynamic_css          Wiz Dynamic CSS.
 * @return string
 */
function wiz_topbar_dynamic_css( $dynamic_css ) {
            
            //Top Bar Header
            $topbar_spacing              = wiz_get_option( 'topbar-padding' );
            $topbar_bg_color             = wiz_get_option( 'topbar-bg-color' );
            $topbar_link_color         = wiz_get_option( 'topbar-link-color' );
			$topbar_link_h_color       = wiz_get_option( 'topbar-link-h-color' );
			$topbar_text_color         = wiz_get_option( 'topbar-text-color' );
			$topbar_border_color       = wiz_get_option( 'topbar-border-color' );
			$topbar_border_size        = wiz_get_option( 'topbar-border-size' );

			//Top Bar Header SubMenu
			$topbar_submenu_bg_color   = wiz_get_option( 'topbar-submenu-bg-color' );
			$topbar_submenu_items_color   = wiz_get_option( 'topbar-submenu-items-color' );
			$topbar_submenu_items_h_color   = wiz_get_option( 'topbar-submenu-items-h-color' );
            $top_bar_content_align          = wiz_get_option( 'top-bar-content-align' );
			$topbar_font_size                    = wiz_get_option( 'topbar-font-size' );
            
            $css_content = array(     
                '.wiz-top-header-section-wrap .wiz-top-header-section' => array(
                    'justify-content' => wiz_get_option( 'top-bar-content-align' ),
                ),
                // Top Bar Header   topbar-bg-color
                '.wiz-top-header'  => array(
                    'padding-top'    => wiz_responsive_spacing( $topbar_spacing, 'top', 'desktop' ),
                    'padding-right'  => wiz_responsive_spacing( $topbar_spacing, 'right', 'desktop' ),
                    'padding-bottom' => wiz_responsive_spacing( $topbar_spacing, 'bottom', 'desktop' ),
                    'padding-left'   => wiz_responsive_spacing( $topbar_spacing, 'left', 'desktop' ),  
                    'background-color' => esc_attr($topbar_bg_color),
                    'border-style' => 'solid',
					'border-bottom-color'     => esc_attr( $topbar_border_color),
				    //'border-bottom-width' => wiz_get_css_value( $topbar_border_bottom_size , 'px' ),
                    'border-top-width'    => wiz_responsive_spacing( $topbar_border_size, 'top', 'desktop' ),
                    'border-right-width'  => wiz_responsive_spacing( $topbar_border_size, 'right', 'desktop' ),
                    'border-bottom-width' => wiz_responsive_spacing( $topbar_border_size, 'bottom', 'desktop' ),
                    'border-left-width'   => wiz_responsive_spacing( $topbar_border_size, 'left', 'desktop' ), 
                    
					'font-size'    => wiz_responsive_slider( $topbar_font_size, 'desktop' ),
					'color'          => esc_attr($topbar_text_color),
                ),
                '.wiz-top-header a'  => array(
					'color' => esc_attr( $topbar_link_color ),
				),

				'.wiz-top-header a:hover'  => array(
					'color' => esc_attr( $topbar_link_h_color ),
				),
				'.top-navigation ul.sub-menu'  => array(
					'background-color' => esc_attr( $topbar_submenu_bg_color),
				),
				'.top-navigation ul.sub-menu li a'  => array(
					'color' => esc_attr( $topbar_submenu_items_color),
				),
				'.top-navigation ul.sub-menu li:hover a'   => array(
					'color' => esc_attr( $topbar_submenu_items_h_color),
				),
                 
            );

            $parse_css = wiz_parse_css( $css_content );
            
            $css_tablet = array(
                '.wiz-top-header'  => array(
                    'padding-top'    => wiz_responsive_spacing( $topbar_spacing, 'top', 'tablet' ),
                    'padding-right'  => wiz_responsive_spacing( $topbar_spacing, 'right', 'tablet' ),
                    'padding-bottom' => wiz_responsive_spacing( $topbar_spacing, 'bottom', 'tablet' ),
                    'padding-left'   => wiz_responsive_spacing( $topbar_spacing, 'left', 'tablet' ),  
                    'font-size'    => wiz_responsive_slider( $topbar_font_size, 'tablet' ),
                ),
             );
           $parse_css .= wiz_parse_css( $css_tablet, '', '768' );
            
            $css_mobile = array(
                '.wiz-top-header'  => array(
                    'padding-top'    => wiz_responsive_spacing( $topbar_spacing, 'top', 'mobile' ),
                    'padding-right'  => wiz_responsive_spacing( $topbar_spacing, 'right', 'mobile' ),
                    'padding-bottom' => wiz_responsive_spacing( $topbar_spacing, 'bottom', 'mobile' ),
                    'padding-left'   => wiz_responsive_spacing( $topbar_spacing, 'left', 'mobile' ),
                    'font-size'    => wiz_responsive_slider( $topbar_font_size, 'mobile' ),
                ),
             );
           $parse_css .= wiz_parse_css( $css_mobile, '', '544' );
            
            return $dynamic_css . $parse_css;
}