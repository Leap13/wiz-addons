<?php
/**
 * Top Bar Section
 * 
 * @package Wiz Addons
 */
if (! class_exists('Wiz_Top_Bar_Partials')) {

    /**
     * Top Bar Section
     *
     * @since 1.0.0
     */
    class Wiz_Top_Bar_Partials
    {

        private static $instance;

        /**
         * Initiator
         */
        
        public static function get_instance()
        {
            if (! isset(self::$instance)) {
                self::$instance = new self();
            }
            return self::$instance;
        }
        /**
		 *  Constructor
		 */
		public function __construct() {
            add_action( 'wiz_sitehead_top' , array( $this, 'wiz_top_header_template' ), 9 );
            add_action( 'wiz_get_css_files', array( $this, 'add_styles' ) );
        }

        public function wiz_top_header_template() {
            
			if ( apply_filters( 'wiz_top_bar_enabled', true ) ) {
                wizaddons_get_template( 'top-bar-section/templates/topbar-layout.php' );
            }
            
        }

        function add_styles() {
            Wiz_Style_Generator::wiz_add_css( WIZ_TOPBAR_DIR.'assets/css/minified/style.min.css');

	    }

    }
}
Wiz_Top_Bar_Partials::get_instance();
