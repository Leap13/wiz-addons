(function ($) {
    /**
      * Top Bar Header
      */

    wiz_css('wiz-settings[topbar-bg-color]', 'background-color', '.wiz-top-header');

	/**
     * Top Bar Header Spacing
     */
    wiz_responsive_spacing('wiz-settings[topbar-padding]', '.wiz-top-header', 'padding', ['top', 'bottom', 'right', 'left']);

	/**
	 * Top Bar Header Link Color
	 */
    wiz_css('wiz-settings[topbar-link-color]', 'color', '.wiz-top-header a');
    wiz_css('wiz-settings[topbar-submenu-bg-color]', 'background-color', '.top-navigation ul.sub-menu');
    wiz_css('wiz-settings[topbar-link-h-color]', 'color', '.wiz-top-header a:hover');
    wiz_css('wiz-settings[topbar-text-color]', 'color', '.wiz-top-header');
    wiz_css('wiz-settings[topbar-submenu-items-color]', 'color', '.top-navigation ul.sub-menu li a');
    wiz_css('wiz-settings[topbar-submenu-items-h-color]', 'color', '.top-navigation ul.sub-menu li:hover a');
    wiz_css('wiz-settings[top-bar-content-align]', 'justify-content', '.wiz-top-header-section-wrap .wiz-top-header-section');
    wp.customize('wiz-settings[topbar-border-bottom-size]', function (value) {
        value.bind(function (border) {
            var dynamicStyle = '.wiz-top-header{ border-width: ' + border + 'px }';
            wiz_add_dynamic_css('topbar-border-bottom-size', dynamicStyle);
        });
    });

    wp.customize('wiz-settings[topbar-border-bottom-color]', function (value) {
        value.bind(function (border_color) {
            jQuery('.wiz-top-header').css('border-color', border_color);
        });
    });

})(jQuery);
