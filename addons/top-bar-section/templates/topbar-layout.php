<?php
/**
 * Template for Top Bar Layouout
 * @package     Wiz
 * @author      Wiz
 * @copyright   Copyright (c) 2019, Wiz
 * @link        https://wiz.io/
 * @since       Wiz 1.0.0
 */
if ( ! function_exists( 'wiz_get_top_section' ) ) {

	/**
	 * Function to get top section
	 * @param string $section
	 * @return mixed
	 */
	function wiz_get_top_section( $option ) {

		 $output  = '';
		 $section = wiz_get_option( $option );   
		  if ( is_array( $section ) ) {
			
			foreach ( $section as $sectionnn ) {

				switch ( $sectionnn ) {

			case 'search':
					$output .= wiz_get_search();
				break;

            case 'menu':
					$output .= wiz_get_top_menu();
				break;

			case 'widget':
					$output .= wiz_get_custom_widget($option);
			break;

			case 'text-html':
					$output .= wiz_get_custom_html( $option . '-html' );
			break;
			}
		}
			return $output;			
	}
	}
}
$section_1 = wiz_get_top_section( 'top-section-1' );
$section_2 = wiz_get_top_section( 'top-section-2' );
$section1_class = 'wiz-col-md-6';
$section2_class = 'wiz-col-md-6';
$sections  = 0;


if($section_1 != '' && $section_2 == ''){
	$section1_class = 'wiz-col-md-12';
	$section2_class = 'wiz-col-md-6';
}elseif($section_2 != '' && $section_1 == ''){
	$section1_class = 'wiz-col-md-6';
	$section2_class = 'wiz-col-md-12';
}else{
	$section1_class = 'wiz-col-md-6';
	$section2_class = 'wiz-col-md-6';
}
 if ( '' != $section_1 ) {
	 $sections++;
 }
 if ( '' != $section_2 ) {
 	$sections++;
 } 

if ( empty( $section_1 ) && empty( $section_2 ) ) {
	return;
}

$classes = wiz_get_option( 'topbar-responsive' );
if(in_array('search' , wiz_get_option( 'top-section-1' )) || in_array('search' , wiz_get_option( 'top-section-2' ))){
	$search_style = wiz_get_option('top-bar-search-style');
	$classes .= ' top-bar-' . $search_style;
}
?>

<div class="wiz-top-header-wrap" >
	<div class="wiz-top-header  <?php echo esc_attr( $classes ); ?>" >
		<div class="wiz-container">
			<div class="wiz-row wiz-flex wiz-top-header-section-wrap">
					<div class="wiz-top-header-section wiz-top-header-section-1 wiz-flex wiz-justify-content-flex-start mt-topbar-section-equally <?php echo $section1_class; ?> wiz-col-xs-12" >
							<?php echo $section_1; ?>
					</div>

					<div class="wiz-top-header-section wiz-top-header-section-2 wiz-flex wiz-justify-content-flex-end mt-topbar-section-equally <?php echo $section2_class; ?> wiz-col-xs-12<" >
							<?php echo $section_2; ?>
					</div>
			</div>
		</div><!-- .wiz-container -->
	</div><!-- .wiz-top-header -->
</div><!-- .wiz-top-header-wrap -->