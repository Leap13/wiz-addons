<?php
/**
 * Top Bar Section Customizer
 * 
 * @package Wiz Addons
 */

$wp_customize->add_setting(
	WIZ_THEME_SETTINGS . '[top-section-1]', array(
		'default'           => wiz_get_option( 'top-section-1' ),
		'type'              => 'option',
		'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_multi_choices' ),
	)
);
$wp_customize->add_control(
			new Wiz_Control_Sortable(
		$wp_customize, WIZ_THEME_SETTINGS . '[top-section-1]', array(
		'type'     => 'wiz-sortable',
		'section'  => 'section-topbar-header',
		'priority' => 5,
		'label'    => __( 'Top Section 1', 'wiz-addons' ),
		'choices'  => array(
				'search'    => __( 'Search', 'wiz-addons' ),
				'menu' => __( 'Menu', 'wiz-addons' ),
				'widget'    => __( 'Widget', 'wiz-addons' ),
				'text-html' => __( 'Text / HTML', 'wiz-addons' ),
		),
	)
	)
	);

	/**
	 * Option: Right Section Text / HTML
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[top-section-1-html]', array(
			'default'           => wiz_get_option( 'top-section-1-html' ),
			'type'              => 'option',
			//'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_html' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[top-section-1-html]', array(
			'type'     => 'textarea',
			'section'  => 'section-topbar-header',
			'priority' => 10,
			'label'    => __( 'Custom Text / HTML', 'wiz-addons' ),
			'active_callback' => 'wiz_top_bar_section1_has_html',
		)
	);

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			WIZ_THEME_SETTINGS . '[topbar-section-1-html]', array(
				'selector'            => '.wiz-top-header-section-1',
				'container_inclusive' => true,
				'render_callback'     => array( 'Wiz_Customizer_Partials', '_render_topbar_section_1_html' ),
			)
		);
	}

$wp_customize->add_setting(
	WIZ_THEME_SETTINGS . '[top-section-2]', array(
		'default'           => wiz_get_option( 'top-section-2' ),
		'type'              => 'option',
		'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_multi_choices' ),
	)
);
$wp_customize->add_control(
			new Wiz_Control_Sortable(
		$wp_customize, WIZ_THEME_SETTINGS . '[top-section-2]', array(
		'type'     => 'wiz-sortable',
		'section'  => 'section-topbar-header',
		'priority' => 15,
		'label'    => __( 'Top  Section 2', 'wiz-addons' ),
		'choices'  => 
			array(
				'search'    => __( 'Search', 'wiz-addons' ),
				'menu' => __( 'Menu', 'wiz-addons' ),
				'widget'    => __( 'Widget', 'wiz-addons' ),
				'text-html' => __( 'Text / HTML', 'wiz-addons' ),
		),
	)
					)
);

	/**
	 * Option: Right Section Text / HTML
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[top-section-2-html]', array(
			'default'           => wiz_get_option( 'top-section-2-html' ),
			'type'              => 'option',
			//'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_html' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[top-section-2-html]', array(
			'type'     => 'textarea',
			'section'  => 'section-topbar-header',
			'priority' => 20,
			'label'    => __( 'Custom Text / HTML', 'wiz-addons' ),
			'active_callback' => 'wiz_top_bar_section2_has_html',
		)
	);

		if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			WIZ_THEME_SETTINGS . '[top-section-2-html]', array(
				'selector'            => '.wiz-top-header-section-2',
				'container_inclusive' => true,
				'render_callback'     => array( 'Wiz_Customizer_Partials', '_render_topbar_section_2_html' ),
			)
		);
	}
    
    
    /**
    * Option - Top Bar Spacing
    */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[topbar-padding]', array(
			'default'           => wiz_get_option( 'topbar-padding' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_spacing' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Responsive_Spacing(
			$wp_customize, WIZ_THEME_SETTINGS . '[topbar-padding]', array(
				'type'           => 'wiz-responsive-spacing',
				'section'        => 'section-topbar-header',
				'priority'       => 30,
				'label'          => __( 'Top Bar Padding', 'wiz-addons' ),
				'linked_choices' => true,
				'unit_choices'   => array( 'px', 'em', '%' ),
				'choices'        => array(
						'top'    => __( 'Top', 'wiz-addons' ),
						'right'  => __( 'Right', 'wiz-addons' ),
						'bottom' => __( 'Bottom', 'wiz-addons' ),
						'left'   => __( 'Left', 'wiz-addons' ),
				),
			)
		)
	);
    
    	/**
	 * Option: Top Bar Header Background
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[topbar-bg-color]', array(
			'default'           => wiz_get_option( 'topbar-bg-color' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[topbar-bg-color]', array(
                'priority'       => 37,
                'section' => 'section-topbar-header',
				'label'   => __( 'Top Bar Background Color', 'wiz-addons' ),
			)
		)
	);

	/**
	 * Option: Top Bar Font Size
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[topbar-font-size]', array(
			'default'           => '',
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Responsive_Slider(
			$wp_customize, WIZ_THEME_SETTINGS . '[topbar-font-size]', array(
				'type'           => 'wiz-responsive-slider',
				'section'        => 'section-topbar-header',
				'priority'       => 35,
				'label'          => __( 'Top Bar Font Size', 'wiz' ),
				'unit_choices'   => array(
					'px' => array(
						'min' => 1,
						'step' => 1,
						'max' =>200,
					),
					'em' => array(
						'min' => 0.1,
						'step' => 0.1,
						'max' => 10,
					),
				 ),
			)
		)
	);
	/**
     * Option:Top Bar Responsive
     */
    $wp_customize->add_setting(
			WIZ_THEME_SETTINGS . '[topbar-responsive]',array(
					'default'           => wiz_get_option('topbar-responsive'),
					'type'              => 'option',
					'sanitize_callback' => array('Wiz_Customizer_Sanitizes','sanitize_choices')
			)
	);
	$wp_customize->add_control(
			WIZ_THEME_SETTINGS . '[topbar-responsive]' ,array(
					'priority'   => 36,
					'section'    => 'section-topbar-header',
					'type'     => 'select',
					'label'    => __( 'Top Bar Visibility', 'wiz-addons' ),
					'choices'  => array(
							'all-devices'        => __( 'Show On All Devices', 'wiz-addons' ),
							'hide-tablet'        => __( 'Hide On Tablet', 'wiz-addons' ),
							'hide-mobile'        => __( 'Hide On Mobile', 'wiz-addons' ),
							'hide-tablet-mobile' => __( 'Hide On Tablet & Mobile', 'wiz-addons' ),
					),
			)
	);


	/**
  * Option:Top Bar Text Color
  */
	  $wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[topbar-text-color]', array(
			'default'           => '',
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[topbar-text-color]', array(
				'label'   => __( 'Top Bar Text Color', 'wiz-addons' ),
				'priority'       => 37,
				'section' => 'section-topbar-header',
			)
		)
	);

	 /**
      * Option:Top Bar Link Color
      */
	  $wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[topbar-link-color]', array(
			'default'           => '',
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[topbar-link-color]', array(
				'label'   => __( 'Top Bar Link Color', 'wiz-addons' ),
				'priority'       => 50,
				'section' => 'section-topbar-header',
			)
		)
	);

	/**
      * Option:Top Bar Link Hover Color
      */
	  $wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[topbar-link-h-color]', array(
			'default'           => '',
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[topbar-link-h-color]', array(
				'label'   => __( 'Top Bar Link Hover Color', 'wiz-addons' ),
				'priority'       => 55,
				'section' => 'section-topbar-header',
			)
		)
	);
        
    /**
    * Option - Top Bar Spacing
    */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[topbar-border-size]', array(
			'default'           => wiz_get_option( 'topbar-padding' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_spacing' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Responsive_Spacing(
			$wp_customize, WIZ_THEME_SETTINGS . '[topbar-border-size]', array(
				'type'           => 'wiz-responsive-spacing',
				'section'        => 'section-topbar-header',
				'priority'       => 30,
				'label'          => __( 'Top Bar Border Size', 'wiz-addons' ),
				'linked_choices' => true,
				'unit_choices'   => array( 'px', 'em'),
				'choices'        => array(
						'top'    => __( 'Top', 'wiz-addons' ),
						'right'  => __( 'Right', 'wiz-addons' ),
						'bottom' => __( 'Bottom', 'wiz-addons' ),
						'left'   => __( 'Left', 'wiz-addons' ),
				),
			)
		)
	);

	/**
	 * Option: Top Bar Border Bottom Color
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[topbar-border-bottom-color]', array(
			'default'           => '',
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[topbar-border-bottom-color]', array(
				'section'  => 'section-topbar-header',
				'priority' => 65,
				'label'    => __( 'Top Bar Border Bottom Color', 'wiz-addons' ),
			)
		)
	);

	/**
	 * Option:Top Bar SubMenu Background Color
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[topbar-submenu-bg-color]', array(
			'default'           => '',
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[topbar-submenu-bg-color]', array(
				'priority'       => 70,
				'section' => 'section-topbar-header',
				'label'   => __( 'Top Bar SubMenu Background Color', 'wiz-addons' ),
			)
		)
	);


	/**
	 * Option:Top Bar SubMenu Items Color
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[topbar-submenu-items-color]', array(
			'default'           => '',
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[topbar-submenu-items-color]', array(
				'priority'       => 75,
				'section' => 'section-topbar-header',
				'label'   => __( 'Top Bar SubMenu Items Color', 'wiz-addons' ),
			)
		)
	);


	/**
	 * Option:Top Bar SubMenu Items Hover Color
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[topbar-submenu-items-h-color]', array(
			'default'           => '',
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[topbar-submenu-items-h-color]', array(
				'priority'       => 80,
				'section' => 'section-topbar-header',
				'label'   => __( 'Top Bar SubMenu Items Hover Color', 'wiz-addons' ),
			)
		)
	);
	
	/**
	 * Option: Search Style
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[top-bar-search-style]', array(
			'default'           => 'search-box',
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[top-bar-search-style]', array(
			'type'     => 'select',
			'section'  => 'section-topbar-header',
			'priority' => 85,
			'label'    => __( 'Search Style', 'wiz-addons' ),
			'choices'  => array(
				'search-box'    => __( 'Search Box', 'wiz-addons' ),
				'search-icon'   => __( 'Icon', 'wiz-addons' ),
			),
		)
	);

	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[top-bar-content-align]', array(
			'default'           => '',
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Icon_Select(
			$wp_customize, WIZ_THEME_SETTINGS . '[top-bar-content-align]', array(
				'priority'       => 10,
				'section' => 'section-topbar-header',
				'label'   => __( 'Top Bar Content Align', 'wiz-addons' ),
				'choices'  => array(
					'flex-start' => array(
						'icon' => 'dashicons-editor-alignleft'
					),
					'center' => array(
						'icon' => 'dashicons-editor-aligncenter'
					),
					'flex-end' => array(
						'icon' => 'dashicons-editor-alignright'
					),	
				),
			)
		)
	);