<?php
/**
 * Top Bar Section
 * 
 * @package Wiz Addons
 */

define( 'WIZ_TOPBAR_DIR', WIZ_ADDONS_DIR . 'addons/top-bar-section/' );
define( 'WIZ_TOPBAR_URL', WIZ_ADDONS_URL . 'addons/top-bar-section/' );

if ( ! class_exists( 'Wiz_Topbar' ) ) {

	/**
	 * Meta Box Markup Initial Setup
	 *
	 * @since 1.0.0
	 */
	class Wiz_Topbar {

		/**
		 * Member Variable
		 *
		 * @var object instance
		 */
		private static $instance;

		/**
		 *  Initiator
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 *  Constructor
		 */
		
		public function __construct() {

            require_once WIZ_TOPBAR_DIR . 'classes/class-top-bar-section-settings.php';
			require_once WIZ_TOPBAR_DIR . 'classes/class-top-bar-section-partials.php';
		
            if ( ! is_admin() ) {
				require_once WIZ_TOPBAR_DIR . 'classes/dynamic.css.php';
			}
		}
       
		

	}
    Wiz_Topbar::get_instance();
}

/**
*  Kicking this off by calling 'get_instance()' method
*/
