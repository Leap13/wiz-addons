=== Wiz Addons ===
Contributors: Leap13
Tags: metabox, Wiz, addons
Requires at least: 4.7
Tested up to: 5.2
Requires PHP: 5.4
Stable tag: 0.1.0
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

== Description ==

Wiz Addons plugin adds more features to Wiz WordPress Theme like metaboxes, activate/deactivate the customizer options. More options will be released soon.
This plugin works only with: [Wiz WordPress Theme](https://wiz.io/) So you must install and activate it first.

== Installation ==

1. Install using the WordPress built-in Plugin installer, or Extract the zip file and drop the contents in the `wp-content/plugins/` directory of your WordPress installation.
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Done!

== Frequently Asked Questions ==

= I installed the Wiz Addons Plugin but it does not work =

Make sure that Wiz WordPress Theme is Installed and activated first as this plugin will work only with it.

== Resources ==
* Breadcrumbs https://themehybrid.com/plugins/breadcrumb-trail, @author: Justin Tadlock <justin@justintadlock.com>
 @copyright: Copyright (c) 2008 - 2017, Justin Tadlock @license: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

== Changelog ==

0.1.0 version
- Initial Release

0.1.1 version
- Fixed: compatibility problems with PHP7